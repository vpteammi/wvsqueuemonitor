package com.ptc.wvs.monitor.server;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.ptc.ccp.vertx.utils.JsonTools;
import com.ptc.ccp.vertxservices.JsonRestService;
import com.ptc.wvs.monitor.client.EpmAuthoringAppType;
import com.ptc.wvs.monitor.client.EpmDocumentInfo;
import com.ptc.wvs.monitor.client.JsonHelper;
import com.ptc.wvs.monitor.client.PublishQueuesDataSource;
import com.ptc.wvs.monitor.client.QueueEntryInfo;
import com.ptc.wvs.monitor.client.QueueInfo;
import com.ptc.wvs.monitor.client.StatusInfoCode;
import com.ptc.wvs.monitor.client.WorkerInfo;
import com.sm.javax.langx.Strings;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public class QueueEntryUpdateService extends JsonRestService
{
	
	static {
		ApplicationProperties.processStaticProperties (QueueEntryUpdateService.class);
	}
	
	private static Logger logger = LoggerFactory.getLogger (QueueEntryUpdateService.class);
	
	private Consumer<QueueEntryInfo>       statusChangeNotifier;
	private BiConsumer<String, WorkerInfo> workerEventNotifier;
	private PublishQueuesDataSource        dataSource;
	private Runnable                       startCompleteListener;

	public QueueEntryUpdateService (int port)
	{
		super (port);
	}
	
	public QueueEntryUpdateService statusChangeNotifier (Consumer<QueueEntryInfo> statusChangeNotifier)
	{
		this.statusChangeNotifier = statusChangeNotifier;
		return this;
	}
	
	public QueueEntryUpdateService workerEventNotifier (BiConsumer<String, WorkerInfo> workerEventNotifier)
	{
		this.workerEventNotifier = workerEventNotifier;
		return this;
	}
	
	public QueueEntryUpdateService setDataSource (PublishQueuesDataSource dataSource)
	{
		this.dataSource = dataSource;
		return this;
	}
	
	public QueueEntryUpdateService setStartCompleteListener (Runnable startCompleteListener)
	{
		this.startCompleteListener = startCompleteListener;
		return this;
	}
	
	@Override
	protected JsonObject buildJsonCommand (RoutingContext routingContext)
	{
		JsonObject json = super.buildJsonCommand (routingContext); 
		MultiMap params = routingContext.request ().params ();
		for (Entry<String, String> param : params.entries ()) {
			Object value = param.getValue ();
			String key = param.getKey ();
			if (value != null && ! "".equals (value)) {
				value = parseValue (value);
				json.put (key, value);
			}
		}
		return json;
	}

	@Override
	protected void initOnStart ()
	{
		// TODO Auto-generated method stub

	}
	
	@Override
	protected void onStartComplete ()
	{
		if (startCompleteListener != null) {
			startCompleteListener.run ();
		}
	}

	@Override
	protected void executeRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		logger.info ("Process request: " + cmd.toString ());
		if (cmd.containsKey ("_command")) {
			processCommand (cmd.getString ("_command"), cmd, handler);
		} 
		else if (cmd.containsKey ("typeId")) {
			String typeId = cmd.getString ("typeId");
			switch (typeId) {
				case "WCTYPE|wt.queue.QueueEntry": 
					processQueueEntryInfoRequest (cmd, handler);
					break;
				case "WCTYPE|com.ptc.wvs.server.cadagent.Worker":
					processWorkerEventRequest (cmd, handler);
					break;
			}
		}
		else {
			handler.handle (JsonTools.buildFail ("Command doesn't conatin neither 'typeId' nor '_command' attribute."));
		}
	}
	
	private void processQueueEntryInfoRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		QueueEntryInfo qentry = new QueueEntryInfo (cmd);
		if (statusChangeNotifier != null) {
			statusChangeNotifier.accept (qentry);
		}
		handler.handle (JsonTools.buildSuccess (qentry.getId ()));
	}
	
	private void processWorkerEventRequest (JsonObject cmd, Handler<JsonObject> handler)
	{
		String eventType = cmd.getString ("eventType");
		JsonObject json = cmd.getJsonObject ("worker");
		if (json != null) {
			WorkerInfo worker = new WorkerInfo (json);
			if (workerEventNotifier != null) {
				workerEventNotifier.accept (eventType, worker);
			}
			handler.handle (JsonTools.buildSuccess (worker.getDisplayName ()));
		}
		else {
			handler.handle (JsonTools.buildFail ("Worker object is null or absent"));
		}
	}

	private void processCommand (String command, JsonObject cmd, Handler<JsonObject> handler)
	{
		switch (command) {
			case "queuesinfo":
				processGetQueuesInfoCommand (handler);
				break;
			case "allqueues": 
				processGetAllQueuesCommand (handler);
				break;
			case "queue":
				processGetQueueCommand (cmd.getString ("queuename"), handler);
				break;
			case "document":
				processGetDocumentCommand (cmd.getString ("docid"), handler);
				break;
			case "config":
				processConfigCommand (cmd, handler);
				break;
			case "entries":
				processGetQueueEntriesCommand (cmd, handler);
				break;
			default:
				handler.handle (JsonTools.buildFail ("Unknown command '" + command + "'").put ("help", generateHelp ()));
		}
	}
	
	private JsonArray generateHelp ()
	{
		JsonArray resp = new JsonArray ();
		resp.add (new JsonObject ().put ("command", "queuesinfo")
					.put ("params", "none")
					.put ("documentation", "Returns short information about all non-empty queues")
				);
		resp.add (new JsonObject ().put ("command", "allqueues")
				.put ("params", "none")
				.put ("documentation", "Returns all non-empty queues with all queue entries")
			);
		resp.add (new JsonObject ().put ("command", "queue")
				.put ("params", "queuename")
				.put ("documentation", "Returns all queue entries for the queue with the given name")
			);
		resp.add (new JsonObject ().put ("command", "document")
				.put ("params", "docid")
				.put ("documentation", "Returns detailed informatgion for the document with the given ID")
			);
		resp.add (new JsonObject ().put ("command", "entries")
				.put ("params", "status|apptype")
				.put ("documentation", "Returns all queue entries with the given status or AuthApp type")
			);
		resp.add (new JsonObject ().put ("command", "config")
				.put ("params", "timeout|waittimeout|errortimeout|maxerrorcount")
				.put ("documentation", "Configurate PublishQueueMonitor: "
						+ "* timeout - refresh queues information every 'timeout' msec;"
						+ "* waittimeout - wait 'waittimeout' msec after receiving notification from WC before sending next request to WC;"
						+ "* errortimeout - wait 'errortimeout' msec after failed request;"
						+ "* maxerrorcount - stop monitor after 'maxerrorcount' consecutive errors.")
			);
		return resp;
	}

	private void processConfigCommand (JsonObject cmd, Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			dataSource.timeout (cmd.getLong ("timeout", 0L));
			dataSource.waitRequestTimeout (cmd.getLong ("waittimeout", 0L));
			dataSource.errorResendTimeout (cmd.getLong ("errortimeout", 0L));
			dataSource.maxConsecutiveErrorCount (cmd.getInteger ("maxerrorcount", 0));
			JsonObject response = JsonTools.buildSuccess ();
			JsonObject configData = new JsonObject ()
										.put ("timeout", dataSource.getTimeout ())
										.put ("waittimeout", dataSource.getWaitRequestTimeout ())
										.put ("errortimeout", dataSource.getErrorResendTimeout ())
										.put ("maxerrorcount", dataSource.getMaxConsecutiveErrorCount ());
			response.put ("configdata", configData);
			handler.handle (response);
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}
	
	private Date getTime (String timeStr)
	{
		Date dt = null;
		if (! Strings.isEmpty (timeStr)) {
			try {
				dt = QueueEntryInfo.DateFormat.parse (timeStr);
			}
			catch (ParseException e) {
				logError (String.format ("failed tp convert string '%s' to date", timeStr), e);
			}
		}
		return dt;
	}
	
	private Predicate<QueueEntryInfo> buildFilter (StatusInfoCode status, EpmAuthoringAppType type, Date afterTime, Date beforeTime)
	{
		return q -> ((status == null || q.getStatusInfoCode () == status) && 
				(type == null || q.getAuthApp () == type) && 
				q.isAfter (afterTime) && q.isBefore (beforeTime));
	}

	private void processGetQueueEntriesCommand (JsonObject cmd, Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			String title = "";
			String delim = "";
			String statusStr = cmd.getString ("status");
			StatusInfoCode status = null;
			if (! Strings.isEmpty (statusStr)) {
				status = StatusInfoCode.findByValue (statusStr);
				title += "status == " + status;
				delim = " && ";
			}
			String appStr = cmd.getString ("apptype");
			EpmAuthoringAppType type = null;
			if (! Strings.isEmpty (appStr)) {
				type = EpmAuthoringAppType.findByValue (appStr);
				title += delim + "type == " + type; 
				delim = " && ";
			}
			Date afterTime = getTime (cmd.getString ("aftertime"));
			if (afterTime != null) {
				title += delim + QueueEntryInfo.DateFormat.format (afterTime) + " < execTime"; 
				delim = " && ";
			}
			Date beforeTime = getTime (cmd.getString ("beforetime"));
			if (beforeTime != null) {
				title += delim + "execTime < " + QueueEntryInfo.DateFormat.format (beforeTime); 
				delim = " && ";
			}
			// ---
			Predicate<QueueEntryInfo> filter = buildFilter (status, type, afterTime, beforeTime);
			List<QueueEntryInfo> queue = dataSource.getQueueEntries (filter);
			JsonObject res = JsonTools.buildSuccess ();
			JsonObject queues = new JsonObject ();
			title = "Conditions: " + title;
			queues.put (title, JsonHelper.QueueToJson (queue));
			res.put ("queues", queues);
			handler.handle (res);
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}

	private void processGetDocumentCommand (String docId, Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			dataSource.getDocumentInfo (docId, doc -> sendDocument (docId, doc, handler), 
					err -> handler.handle (JsonTools.buildFail (err)));
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}

	private void sendDocument (String docId, EpmDocumentInfo doc, Handler<JsonObject> handler)
	{
		JsonObject res = JsonTools.buildSuccess ();
		res.put ("docId", docId);
		res.put ("document", JsonHelper.DocumentToJson (doc));
		handler.handle (res);
	}

	private void processGetQueueCommand (String queueName, Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			List<QueueEntryInfo> queue = dataSource.getQueue (queueName);
			JsonObject res = JsonTools.buildSuccess ();
			JsonObject queues = new JsonObject ();
			queues.put (queueName, JsonHelper.QueueToJson (queue));
			res.put ("queues", queues);
			handler.handle (res);
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}
	
	private void processGetQueuesInfoCommand (Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			HashMap<QueueInfo, List<QueueEntryInfo>> queues = dataSource.getQueues ();
			JsonObject res = JsonTools.buildSuccess ();
			res.put ("queues", JsonHelper.QueuesInfoToJson (queues));
			handler.handle (res);
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}

	private void processGetAllQueuesCommand (Handler<JsonObject> handler)
	{
		if (dataSource != null) {
			HashMap<QueueInfo, List<QueueEntryInfo>> queues = dataSource.getQueues ();
			JsonObject res = JsonTools.buildSuccess ();
			res.put ("queues", JsonHelper.QueuesToJson (queues));
			handler.handle (res);
		}
		else {
			handler.handle (JsonTools.buildFail ("Data source is not set"));
		}
	}
	
}
