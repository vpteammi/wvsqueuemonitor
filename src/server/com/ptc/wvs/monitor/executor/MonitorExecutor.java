package com.ptc.wvs.monitor.executor;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.ptc.ccp.vertxservices.AbstractPTCVerticle;
import com.ptc.ccp.vertxservices.TypedAbstractRestService;
import com.ptc.wvs.monitor.client.PublishQueueEvent;
import com.ptc.wvs.monitor.client.PublishQueueMonitor;
import com.ptc.wvs.monitor.client.WindchillInfoReader;
import com.ptc.wvs.monitor.server.QueueEntryUpdateService;
import com.sm.javax.langx.Strings;
import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;
import com.sm.javax.utilx.ApplicationProperties.CommandLineArgument;
import com.sm.javax.utilx.CommandLineArguments;

import io.vertx.core.json.JsonObject;

public class MonitorExecutor
{

	@SettingProperty (defaultValue="8686", cmdLineName="server-port", documentation="Service HTTP port.")
	private static int WebPort;
	
	@SettingProperty (defaultValue="remoteservice", cmdLineName="server-path", documentation="Service path.")
	private static String WebPath;
	
	@SettingProperty (defaultValue="ddementiev0d1.ptcnet.ptc.com", cmdLineName="wchost", 
			documentation="Windchill host http address (not including port)")
	private static String WindchillHost;
	
	@SettingProperty (defaultValue="10600", cmdLineName="wcport", 
			documentation="Windchill base port number")
	private static int WindchillPort;
	
	@SettingProperty (defaultValue="10620", cmdLineName="wcport", 
			documentation="Windchill worker server port number (default base Windchill port + 20)")
	private static int WindchillWorkerServicePort;
	
	@SettingProperty (defaultValue="wtadmin", cmdLineName="wcuser", documentation="Windchill user name")
	private static String WCuser;
	
	@SettingProperty (defaultValue="wtadmin", cmdLineName="wcpass", documentation="Windchill user password")
	private static String WCpass;
	
	// ===============================================================================================================
	
	private static final String DefaultPropertiesFile = "monitor_executor.properties"; 
	
	public static void main (String[] args)
	{
		String propFile = DefaultPropertiesFile;

		CommandLineArguments arguments = new CommandLineArguments (args);
		if (arguments.hasArgument ("config")) {
			propFile = arguments.getArgument ("config");
		}
		ApplicationProperties.loadProperties (propFile, arguments, MonitorExecutor.class);
		
		if (arguments.hasArgument ("help")) {
			usage (false);
			return;
		}
		if (arguments.hasArgument ("help-properties")) {
			usage (true);
			return;
		}

		// ---------------------------------------------
		System.out.println ("Starting WVS Monitor");
		if (! WebPath.startsWith ("/")) {
			WebPath = "/" + WebPath;
		}
		
		try {
			WindchillInfoReader wcReader = new WindchillInfoReader (WindchillHost, WindchillPort, 
					WindchillWorkerServicePort, WCuser, WCpass);
			PublishQueueMonitor monitor = new PublishQueueMonitor (wcReader);
			CompletableFuture<Boolean> startServiceTask = new CompletableFuture<> ();
			TypedAbstractRestService<JsonObject, JsonObject> service = new QueueEntryUpdateService (WebPort)
					.statusChangeNotifier ((q) -> monitor.statusChangeListener (q))
					.workerEventNotifier ((et, w) -> monitor.workerEventListener (et, w))
					.setDataSource (monitor)
					.setStartCompleteListener (() -> startServiceTask.complete (true))
					.setServerPath (WebPath);
			AbstractPTCVerticle.runClustered (service, AbstractPTCVerticle.buildVertxOptions ());
			try {
				startServiceTask.get ();
				wcReader.setMicroserviceAddress (service.getWebServiceAddress ());
				monitor.start ();
			}
			catch (InterruptedException | ExecutionException e1) {
				System.err.println ("Failed to start QueueEntryUpdateService micro-service.");
			}
			/*
			 * Illustration how to use event listeners
			 */
			monitor.registerEventListener (PublishQueueEvent.NewJobEvent, (e) -> System.out.println ("Event<" + e + ">"));
			monitor.registerEventListener (PublishQueueEvent.JobFinishedEvent, (e) -> System.out.println ("Event<" + e + ">"));
			monitor.registerEventListener (PublishQueueEvent.getQueueIsNotEmptyEvent ("PublisherQueueL", "PublisherQueueM", "PublisherQueueH"), 
					(e) -> System.out.println ("Event<" + e + ">"));
			monitor.registerEventListener (PublishQueueEvent.getWorkerBecameFreeEvent ("neva-PROE:1"), 
					(e) -> System.out.println ("Event<" + e + ">"));
			monitor.registerEventListener (PublishQueueEvent.getWorkerBecameBusyEvent ("neva-PROE:1"), 
					(e) -> System.out.println ("Event<" + e + ">"));
		}
		catch (MalformedURLException e) {
			System.err.println ("Failed to create valid URL for '" + WindchillHost + "' -" + e.getMessage ());
		}
	}
	
	private static void usage (boolean showProperties)
	{
		List<CommandLineArgument> args = ApplicationProperties.collectCommandLineArguments (
				MonitorExecutor.class, WindchillInfoReader.class);
		if (args != null) {
			System.out.println ("Available options:");
			System.out.println ("  -config (String) = \"" + DefaultPropertiesFile + "\" - properties config file");
			System.out.println ("  -help - show all command line arguments");
			System.out.println ("  -help-properties - show all available properties");
			for (CommandLineArgument arg : args) {
				System.out.println ("  " + arg.toString ());
			}
		}
		
		if (showProperties) {
			List<Field> fields = ApplicationProperties.collectStaticProperties (
					MonitorExecutor.class, WindchillInfoReader.class);
			System.out.println ("\nPossible contents of " + DefaultPropertiesFile + " file:");
			for (Field field : fields) {
				SettingProperty ann = field.getAnnotation (SettingProperty.class);
				String doc = ann.documentation ();
				if (! Strings.isEmpty (doc)) {
					System.out.println ("# " + doc);
				}
				String defVal = ann.defaultValue ();
				if (defVal != null && field.getType ().equals (String.class)) {
					defVal = "\"" + defVal + "\"";
				}
				String eqVal = (defVal != null ? "=" + defVal : "");
				System.out.println (field.getDeclaringClass ().getName () + "." + field.getName () + eqVal + "\n");
			}
		}
	}

}
