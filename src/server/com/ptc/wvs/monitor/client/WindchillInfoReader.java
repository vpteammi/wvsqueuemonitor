package com.ptc.wvs.monitor.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.ptc.microservices.clients.HttpClient;
import com.ptc.microservices.clients.HttpClientResponse;
import com.sm.javax.langx.Strings;
import com.sm.javax.netx.BasicNetClient;
import com.sm.javax.query.Result;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class WindchillInfoReader
{
	public static boolean DebugOutput = false;
	
	private static final String HTTP = "http://";
	private static final String HTTPS = "https://";
	
	private static final String PATH = "Windchill/servlet/rest/objects";
	private static final String ENUM_PATH = "/Windchill/servlet/rest/v1/enumeratedtypes";
	private static final String QueueEntryType = "WCTYPE%7Cwt.queue.QueueEntry"; // WCTYPE|wt.queue.QueueEntry 
	private static final String QueueType = "WCTYPE%7Cwt.queue.ProcessingQueue"; // WCTYPE|wt.queue.ProcessingQueue
	private static final String QueueFilter = "startswith(name%2C'Publisher')";
	private static final String PARAM_SPACE = "%20";
	private static final long DefaultTimeout = 5000L;
	
	private String wcAddress;
	private String wcHost;
	private int    wcWorkerCmdPort;
	private String userName;
	private String userPasswd;
	
	private String microserviceAddress;

	private String[] queueEntryFields = QueueEntryInfo.WCAttributes;
	
	private String[] docFields = {"number", "name", "CADName", "version", "authoringApplication"};
	
	private String[] QueueFields = {"name"};
	
	private URL wcUrl;
	private HttpClient     httpClient;
	private BasicNetClient netClient;
	private ScheduledThreadPoolExecutor executor;
	private long timeout = DefaultTimeout;

	private String wcWorkerAddress;


	/*
	 * Request ALL QueueEntry objects from WC:
	 * http://ddementiev0d1.ptcnet.ptc.com:10600/Windchill/servlet/rest/objects?
	 *  %24select=queue.name%2CstatusInfo.code%2CstartExec%2CendExec%2CfailureCount%2CentryOwner.name%2CentryOwner.fullName
	 *  %2CtargetClass%2CtargetMethod%2CtargetClass%2Cargs%5B0%5D.arg.persistableRef
	 * &typeId=WCTYPE%7Cwt.queue.QueueEntry
	 *
	 * Request specific document info
	 * http://ddementiev0d1.ptcnet.ptc.com:10600/Windchill/servlet/rest/objects/
	 *  OR%3Awt.epm.EPMDocument%3A60316              <- this is ID from 'args[0].arg.persistableRef' field from above request
	 *  ?%24select=number%2Cname%2CCADName%2Cversion
	 *  
	 *  Request different queues' info
	 *  http://ddementiev0d1.ptcnet.ptc.com:10600/Windchill/servlet/rest/objects?
	 *   %24select=name
	 *   &typeId=WCTYPE%7Cwt.queue.ProcessingQueue
	 *   &%24filter=startswith(name%2C'Publisher')
	 *   
	 *   Request all possible EPMAuthoringAppType
	 *   http://ddementiev0d1.ptcnet.ptc.com:10600/Windchill/servlet/rest/v1/enumeratedtypes?class=wt.epm.EPMAuthoringAppType
	 */
	
	public WindchillInfoReader (String wcHost, int wcPort, int wcWorkerCmdPort, String userName, String userPasswd) throws MalformedURLException
	{
		super ();
		if (Strings.isEmpty (wcHost)) {
			throw new MalformedURLException ("Windchill host cannot be empty");
		}
		this.wcHost = wcHost.toLowerCase ();
		this.wcAddress = this.wcHost + ":" + wcPort;
		this.wcWorkerAddress = (wcWorkerCmdPort > 0 ? this.wcHost + ":" + wcWorkerCmdPort : null);
		if (! this.wcHost.startsWith (HTTP) && ! this.wcHost.startsWith (HTTPS)) {
			wcAddress = HTTP + wcAddress;
			if (wcWorkerAddress != null) {
				wcWorkerAddress = HTTP + wcWorkerAddress;
			}
		}
		this.wcWorkerCmdPort = wcWorkerCmdPort;
		this.userName = userName;
		this.userPasswd = userPasswd;
		wcUrl = new URL (buildFullAddress ());
		executor = new ScheduledThreadPoolExecutor (1);
		httpClient = new HttpClient (executor);
	}
	
	public void setMicroserviceAddress (String microserviceAddress)
	{
		this.microserviceAddress = microserviceAddress;
	}
	
	public void stop ()
	{
		httpClient.stop ();
	}
	
	// ----------------
	private int queueReqNum = 0;
	public void getQueues (Consumer<HashMap<QueueInfo, List<QueueEntryInfo>>> handler, Consumer<Throwable> errHandler)
	{
		try {
			queueReqNum ++;
			httpClient.get (wcUrl, resp -> readResponse (queueReqNum, resp, handler, errHandler), timeout)
				.setBasicAuthentication (userName, userPasswd)
				.end ();
			logDebug (String.format ("Queue request #%d sent", queueReqNum));
		}
		catch (IOException e) {
			errHandler.accept (e);
		}
	}

	public HashMap<QueueInfo, List<QueueEntryInfo>> getQueues ()
	{
		try {
			CompletableFuture<HashMap<QueueInfo, List<QueueEntryInfo>>> task = new CompletableFuture<> ();
			getQueues (qs -> task.complete (qs), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve Queue Entries from Windchill - " + e.getMessage ());
		}
		return null; 
	}
	
	// ----------------
	public void getQueueEntryJobNumber (int jobNumber, Consumer<List<QueueEntryInfo>> handler, Consumer<Throwable> errHandler)
	{
		try {
			URL url = new URL (buildFullAddress (jobNumber));
			logDebug ("Sending queue entry request to " + url);			
			httpClient.get (url, resp -> readQueueEntryResponse (resp, handler, errHandler), timeout)
				.setBasicAuthentication (userName, userPasswd)
				.end ();
		}
		catch (IOException e) {
			errHandler.accept (e);
		}
	}
	
	public List<QueueEntryInfo> getQueueEntryJobNumber (int jobNumber)
	{
		try {
			CompletableFuture<List<QueueEntryInfo>> task = new CompletableFuture<> ();
			getQueueEntryJobNumber (jobNumber, qs -> task.complete (qs), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve Queue Entry with entryNumber=" + jobNumber + " from Windchill - " + e.getMessage ());
		}
		return null;
	}

	// ----------------
	public void getDocumentInfo (String docId, Consumer<EpmDocumentInfo> handler, Consumer<Throwable> errHandler)
	{
		EpmDocumentInfo doc = EpmDocumentInfo.find (docId);
		if (doc != null) {
			handler.accept (doc);
		}
		else {
			try {
				httpClient.get (getWindchillRestUrl (docId), resp -> readDocumentResponse (resp, handler, errHandler), timeout)
					.setBasicAuthentication (userName, userPasswd)
					.end ();
			}
			catch (IOException e) {
				System.err.println ("Failed to retrieve document info for '" + docId + "' from Windchill - " + e.getMessage ());
			}
		}
	}
	
	public EpmDocumentInfo getDocumentInfo (String docId)
	{
		try {
			CompletableFuture<EpmDocumentInfo> task = new CompletableFuture<> ();
			getDocumentInfo (docId, doc -> task.complete (doc), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve document info for '" + docId + "' from Windchill - " + e.getMessage ());
		}
		return null;
	}

	// ----------------
	public void getPublishQueueList (Consumer<List<QueueInfo>> handler, Consumer<Throwable> errHandler)
	{
		try {
			httpClient.get (getWindchillQueueUrl (), resp -> readQueueResponse (resp, handler, errHandler), timeout)
				.setBasicAuthentication (userName, userPasswd)
				.end ();
		}
		catch (IOException e) {
			errHandler.accept (e);
		}
	}
	
	public List<QueueInfo> getPublishQueueList ()
	{
		try {
			CompletableFuture<List<QueueInfo>> task = new CompletableFuture<> ();
			getPublishQueueList (qs -> task.complete (qs), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve Queues information from Windchill - " + e.getMessage ());
		}
		return null; 
	}
	
	// ----------------
	public void getAuthoringAppTypes (Consumer<List<EpmAuthoringAppType>> handler, Consumer<Throwable> errHandler)
	{
		try {
			httpClient.get (getWindchillAuthTypesUrl (), resp -> readAuthoringTypesResponse (resp, handler, errHandler), timeout)
				.setBasicAuthentication (userName, userPasswd)
				.end ();
		}
		catch (IOException e) {
			errHandler.accept (e);
		}
	}
	
	public List<EpmAuthoringAppType> getAuthoringAppTypes ()
	{
		try {
			CompletableFuture<List<EpmAuthoringAppType>> task = new CompletableFuture<> ();
			getAuthoringAppTypes (qs -> task.complete (qs), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve Queues information from Windchill - " + e.getMessage ());
		}
		return null; 
	}

	// ----------------
	public void getCadWorkers (Consumer<List<WorkerInfo>> handler, Consumer<Throwable> errHandler) 
	{
		if (wcWorkerCmdPort > 0) {
			createNetClient ();
			netClient.start (wcHost, wcWorkerCmdPort, resp -> processWorkersResponse (resp, handler, errHandler))
				.timeout (timeout)
				.send ("INFO")
				.end ();
		}
		else {
			errHandler.accept (new IOException ("Windchill worker port is not set"));
		}
	}

	public List<WorkerInfo> getCadWorkers ()
	{
		try {
			CompletableFuture<List<WorkerInfo>> task = new CompletableFuture<> ();
			getCadWorkers (qs -> task.complete (qs), err -> task.completeExceptionally (err));
			return task.get ();
		}
		catch (InterruptedException | ExecutionException e) {
			System.err.println ("Failed to retrieve CAD Workers from Windchill - " + e.getMessage ());
		}
		return null; 
	}
	
	public void addWorkerEventListener (Consumer<JsonObject> handler, Consumer<Throwable> errHandler)
	{
		if (wcWorkerCmdPort > 0) {
			createNetClient ();
			try {
				if (! Strings.isEmpty (microserviceAddress)) {
					logDebug ("Sendign request to " + wcHost + ":" + wcWorkerCmdPort);
					netClient.start (wcHost, wcWorkerCmdPort, resp -> processRegisterWorkerListenerResponse (resp, handler, errHandler))
						.timeout (timeout)
						.send ("LISTENER ADD " + microserviceAddress)
						.end ();
				}
				else {
					throw new IOException ("Micro-service address is not set");
				}
			}
			catch (IOException e) {
				errHandler.accept (e);
			}
		}
		else {
			errHandler.accept (new IOException ("Windchill Worker port is not set"));
		}
	}
	
	private void createNetClient ()
	{
		if (netClient == null) {
			netClient = new BasicNetClient (executor);
		}
	}

	// ----------------
	public static void printQueues (HashMap<QueueInfo, List<QueueEntryInfo>> queues, PrintStream out)
	{
		if (queues != null) {
			for (QueueInfo queue : queues.keySet ()) {
				List<QueueEntryInfo> list = queues.get (queue);
				out.println (queue + " (" + list.size () + ") = [");
				printQueue (list, "  ", out);
				out.println ("]");
			}
		}
	}

	public static void printQueues (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		printQueues (queues, System.out);
	}
	
	public static void printQueue (List<QueueEntryInfo> list, String indent, PrintStream out)
	{
		for (QueueEntryInfo entry : list) {
			out.println (indent + entry.toString ());
		}
	}
	
	public static void printQueue (List<QueueEntryInfo> list, String indent)
	{
		printQueue (list, indent, System.out);
	}
	
	// -----------------------------------------------------------
	private void processAnyResponse (HttpClientResponse resp, Consumer<String> responseProcessor, Consumer<Throwable> errHandler)
	{
		int respCode = resp.getResponseCode ();
		if (respCode == 200) {
			String body = resp.getBody ();
			responseProcessor.accept (body);
		}
		else if (respCode == 401) {
			errHandler.accept (new IOException ("login authorization failed (response code = " + respCode + ")"));
		}
		else if (respCode == 404) {
			errHandler.accept (new IOException ("specified objects do not exist (response code = " + respCode + ")"));
		}
		else if (respCode == 400) {
			errHandler.accept (new IOException (resp.getBody () +  " (response code = " + respCode + ")"));
		}
		else {
			errHandler.accept (new IOException ("unspecified error (response code = " + respCode + ")"));
		}
	}
	
	private void readResponse (int reqNum, HttpClientResponse resp, Consumer<HashMap<QueueInfo, List<QueueEntryInfo>>> handler,
			Consumer<Throwable> errHandler)
	{
		logDebug (String.format ("Received response #%d", reqNum));
		processAnyResponse (resp, body -> processQueuesResponseBody (body, handler, errHandler), errHandler);
	}
	
	private void processQueuesResponseBody (String body, Consumer<HashMap<QueueInfo, List<QueueEntryInfo>>> handler,
			Consumer<Throwable> errHandler)
	{
		HashMap<QueueInfo, List<QueueEntryInfo>> queues = new HashMap<> ();
		if (DebugOutput) {
			System.out.println (body);
		}
		try {
			JsonArray jsArray = new JsonArray (body);
			if (DebugOutput) {
				System.out.println ("Received info about " + jsArray.size () + " elements");
			}
			for (int i=0; i<jsArray.size (); i++) {
				JsonObject jselem = jsArray.getJsonObject (i);
				addQueueEntry (queues, jselem);
			}
		}
		catch (Exception ex) {
			errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
		}
		handler.accept (queues);
	}
	
	
	private void readQueueEntryResponse (HttpClientResponse resp, Consumer<List<QueueEntryInfo>> handler,
			Consumer<Throwable> errHandler)
	{
		processAnyResponse (resp, body -> processQueueEntryResponseBody (body, handler, errHandler), errHandler);
	}
	
	private void processQueueEntryResponseBody (String body, Consumer<List<QueueEntryInfo>> handler,
			Consumer<Throwable> errHandler)
	{
		List<QueueEntryInfo> entries = new LinkedList<> ();
		if (DebugOutput) {
			System.out.println (body);
		}
		try {
			JsonArray jsArray = new JsonArray (body);
			if (DebugOutput) {
				System.out.println ("Received info about " + jsArray.size () + " elements");
			}
			for (int i=0; i<jsArray.size (); i++) {
				JsonObject json = jsArray.getJsonObject (i);
				entries.add (new QueueEntryInfo (json, this));
			}
		}
		catch (Exception ex) {
			errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
		}
		handler.accept (entries);
	}

	private void readDocumentResponse (HttpClientResponse resp, Consumer<EpmDocumentInfo> handler,
			Consumer<Throwable> errHandler)
	{
		processAnyResponse (resp, body -> processDocumentResponseBody (body, handler, errHandler), errHandler);
	}
	
	private void processDocumentResponseBody (String body, Consumer<EpmDocumentInfo> handler, Consumer<Throwable> errHandler)
	{
		if (DebugOutput) {
			System.out.println (body);
		}
		try {
			JsonObject jselem = new JsonObject (body);
			EpmDocumentInfo document = EpmDocumentInfo.create (jselem);
			handler.accept (document);
		}
		catch (Exception ex) {
			errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
		}
	}

	private void readQueueResponse (HttpClientResponse resp, Consumer<List<QueueInfo>> handler,
			Consumer<Throwable> errHandler)
	{
		processAnyResponse (resp, body -> processQueueResponseBody (body, handler, errHandler), errHandler);
	}

	private void processQueueResponseBody (String body, Consumer<List<QueueInfo>> handler,
			Consumer<Throwable> errHandler)
	{
		if (DebugOutput) {
			System.out.println (body);
		}
		try {
			List<QueueInfo> queues = new ArrayList<> ();
			JsonArray jsArray = new JsonArray (body);
			if (DebugOutput) {
				System.out.println ("Received info about " + jsArray.size () + " elements");
			}
			for (int i=0; i<jsArray.size (); i++) {
				JsonObject json = jsArray.getJsonObject (i);
				queues.add (QueueInfo.create (json));
			}

			handler.accept (queues);
		}
		catch (Exception ex) {
			errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
		}
	}

	private void processWorkersResponse (Result<String> resp, Consumer<List<WorkerInfo>> handler,
			Consumer<Throwable> errHandler)
	{
		if (resp.succeeded ()) {
			String body = resp.value ();
			if (DebugOutput) {
				System.out.println (body);
			}
			try {
				List<WorkerInfo> workers = new ArrayList<WorkerInfo> ();
				JsonArray jsArray = new JsonArray (body);
				if (DebugOutput) {
					System.out.println ("Received info about " + jsArray.size () + " elements");
				}
				for (int i=0; i<jsArray.size (); i++) {
					JsonObject json = jsArray.getJsonObject (i);
					workers.add (new WorkerInfo (json));
				}

				handler.accept (workers);
			}
			catch (Exception ex) {
				errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
			}
		}
		else {
			errHandler.accept (resp.cause ());
		}
	}

	
	private void processRegisterWorkerListenerResponse (Result<String> resp, Consumer<JsonObject> handler,
			Consumer<Throwable> errHandler)
	{
		if (resp.succeeded ()) {
			String body = resp.value ();
			if (DebugOutput) {
				System.out.println (body);
			}
			try {
				JsonObject response = new JsonObject (body);
				handler.accept (response);
			}
			catch (Exception ex) {
				errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
			}
		}
		else {
			errHandler.accept (resp.cause ());
		}
	}

	private void readAuthoringTypesResponse (HttpClientResponse resp, Consumer<List<EpmAuthoringAppType>> handler,
			Consumer<Throwable> errHandler)
	{
		processAnyResponse (resp, body -> processAuthoringTypesResponseBody (body, handler, errHandler), errHandler);
	}

	private void processAuthoringTypesResponseBody (String body, Consumer<List<EpmAuthoringAppType>> handler,
			Consumer<Throwable> errHandler)
	{
		if (DebugOutput) {
			System.out.println (body);
		}
		try {
			List<EpmAuthoringAppType> types = new ArrayList<> ();
			JsonArray jsArray = new JsonArray (body);
			if (DebugOutput) {
				System.out.println ("Received info about " + jsArray.size () + " elements");
			}
			for (int i=0; i<jsArray.size (); i++) {
				JsonObject json = jsArray.getJsonObject (i);
				types.add (EpmAuthoringAppType.create (json));
			}

			handler.accept (types);
		}
		catch (Exception ex) {
			errHandler.accept (new IOException ("Failed to parse JSON object - " + ex.getMessage ()));
		}
	}

	private void addQueueEntry (HashMap<QueueInfo, List<QueueEntryInfo>> queues, JsonObject json)
	{
		addQueueEntry (queues, new QueueEntryInfo (json, this));
	}
	
	private void addQueueEntry (HashMap<QueueInfo, List<QueueEntryInfo>> queues, QueueEntryInfo entry)
	{
		if (entry != null) {
			QueueInfo queue = entry.getQueue ();
			List<QueueEntryInfo> list = queues.get (queue);
			if (list == null) {
				list = new ArrayList<QueueEntryInfo> ();
				queues.put (queue, list);
			}
			list.add (entry);
		}
	}
	
	// -----------------------------------------------------------------------------------------------------
	private URL getWindchillRestUrl (String docId) throws MalformedURLException
	{
		return new URL (buildDocumentFullAddress (docId));
	}

	private String buildDocumentFullAddress (String docId)
	{
		return String.format ("%s/%s/%s?$select=%s", wcAddress, PATH, docId, buildDocumentFieldsList ());
	}

	private String buildFullAddress ()
	{
		return String.format ("%s/%s?$select=%s&typeId=%s", wcAddress, PATH, buildQueueEntryFieldsList (), QueueEntryType);
	}
	
	private String buildFullAddress (int entryNumber)
	{
		return String.format ("%s&$filter=entryNumber%seq%s%d", buildFullAddress (), PARAM_SPACE, PARAM_SPACE, entryNumber);
	}
	
	private String buildQueueEntryFieldsList ()
	{
		return Arrays.stream (queueEntryFields).collect (Collectors.joining ("%2C"));
	}

	private String buildDocumentFieldsList ()
	{
		return Arrays.stream (docFields).collect (Collectors.joining ("%2C"));
	}
	
	private URL getWindchillQueueUrl () throws MalformedURLException
	{
		return new URL (buildWindchillQueueAddress ());
	}
	
	private URL getWindchillAuthTypesUrl () throws MalformedURLException
	{
		return new URL (buildWindchillAuthTypesAddress ());
	}
	
	private String buildWindchillQueueAddress ()
	{
		return String.format ("%s/%s?$select=%s&typeId=%s&$filter=%s", wcAddress, PATH, buildQueueFieldsList (), QueueType, QueueFilter);
	}
	
	private Object buildQueueFieldsList ()
	{
		return Arrays.stream (QueueFields).collect (Collectors.joining ("%2C"));
	}
	
	private String buildWindchillAuthTypesAddress ()
	{
		return String.format ("%s/%s?%s", wcAddress, ENUM_PATH, "class=wt.epm.EPMAuthoringAppType");
	}
	
	private void logDebug (String msg)
	{
		System.out.println (new Date () + ":WindchillInfoReader: " + msg);
	}

}
