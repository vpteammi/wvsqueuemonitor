package com.ptc.wvs.monitor.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class JsonHelper
{
	public static JsonObject QueuesToJson (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		JsonObject json = null;
		if (queues != null) {
			json = new JsonObject ();
			for (Entry<QueueInfo, List<QueueEntryInfo>> entry : queues.entrySet ()) {
				QueueInfo queueInfo = entry.getKey ();
				List<QueueEntryInfo> queue = entry.getValue ();
				json.put (queueInfo.getQueueName (), QueueToJson (queue));
			}
		}
		return json;
	}
	
	public static JsonObject QueuesInfoToJson (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		JsonObject json = null;
		if (queues != null) {
			json = new JsonObject ();
			for (Entry<QueueInfo, List<QueueEntryInfo>> entry : queues.entrySet ()) {
				QueueInfo queueInfo = entry.getKey ();
				List<QueueEntryInfo> queue = entry.getValue ();
				json.put (queueInfo.getQueueName (), (queue != null ? queue.size () : 0));
			}
		}
		return json;
	}

	public static JsonArray QueueToJson (List<QueueEntryInfo> queue)
	{
		JsonArray json = null;
		if (queue != null) {
			json = new JsonArray ();
			for (QueueEntryInfo queueEntry : queue) {
				json.add (queueEntry.toJson ());
			}
		}
		return json;
	}
	
	public static JsonObject DocumentToJson (EpmDocumentInfo doc)
	{
		JsonObject json = null;
		if (doc != null) {
			json = doc.toJson ();
		}
		return json;
	}


}
