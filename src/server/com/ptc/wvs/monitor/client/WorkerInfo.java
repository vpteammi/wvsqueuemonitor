package com.ptc.wvs.monitor.client;

import java.util.Date;

import com.sm.javax.langx.Strings;

import io.vertx.core.json.JsonObject;

public class WorkerInfo
{
	
	private String remotePath;
	private String displayName;
	private String distTransferPath;
	private String distExe;
	private String type;
	private String host;
	private String job;
	private String protocol;
	private String hostType;
	private boolean enabled;
	private boolean connected;
	private boolean offline;
	private boolean busy;
	private boolean starting;
	private boolean toBeStopped;
	private boolean failedToStart;
	private int port;
	private int jobNumber;
	private int entryNumber;
	private Date jobStart;
	private Date jobStop;

	public WorkerInfo (JsonObject json)
	{
		super ();
		remotePath = json.getString ("remotePath");
		displayName = json.getString ("displayName");
		distTransferPath = json.getString ("distTransferPath");
		distExe = json.getString ("distExe");
		type = json.getString ("type");
		host = json.getString ("host");
		job = json.getString ("job");
		enabled = json.getBoolean ("enabled", false);
		connected = json.getBoolean ("connected", false);
		offline = json.getBoolean ("offline", false);
		busy = json.getBoolean ("busy", false);
		starting = json.getBoolean ("starting", false);
		toBeStopped = json.getBoolean ("toBeStopped", false);
		port = json.getInteger ("port", 0);
		jobNumber = json.getInteger ("jobNumber", 0);
		protocol = json.getString ("protocol");
		hostType = json.getString ("hostType");
		long jobStartTime = json.getLong ("jobStart", -1L);
		if (jobStartTime != -1) {
			jobStart = new Date (jobStartTime);
		}
		long jobStopTime = json.getLong ("jobStop", -1L);
		if (jobStopTime != -1) {
			jobStop = new Date (jobStopTime);
		}
		if (json.containsKey ("workOrder")) {
			JsonObject workOrder = json.getJsonObject ("workOrder");
			if (workOrder.containsKey ("id")) {
				String workOrderId = workOrder.getString ("id");
				if (! Strings.isEmpty (workOrderId)) {
					try {
						entryNumber = Integer.parseInt (workOrderId);
					}
					catch (NumberFormatException ex) {
						entryNumber = 0;
						System.err.println ("Failed to convert work.workOrderId '" + workOrderId +"' to integer.");
					}
				}
			}
		}
		failedToStart = json.getBoolean ("failedToStart", false);
	}

	public String getRemotePath ()
	{
		return remotePath;
	}

	public String getDisplayName ()
	{
		return displayName;
	}

	public String getDistTransferPath ()
	{
		return distTransferPath;
	}

	public String getDistExe ()
	{
		return distExe;
	}

	public String getType ()
	{
		return type;
	}

	public String getHost ()
	{
		return host;
	}

	public String getJob ()
	{
		return job;
	}

	public boolean isEnabled ()
	{
		return enabled;
	}

	public boolean isConnected ()
	{
		return connected;
	}

	public boolean isOffline ()
	{
		return offline;
	}

	public boolean isBusy ()
	{
		return busy;
	}

	public boolean isStarting ()
	{
		return starting;
	}

	public boolean isToBeStopped ()
	{
		return toBeStopped;
	}

	public int getPort ()
	{
		return port;
	}

	public int getJobNumber ()
	{
		return jobNumber;
	}
	
	public int getEntryNumber ()
	{
		return entryNumber;
	}
	
	@Override
	public String toString ()
	{
		StringBuilder sb = new StringBuilder (displayName);
		sb.append (": ").append (type).append (" on ").append (protocol).append ("//").append (host).append (":").append (port)
			.append (" (busy: ").append (busy)
			.append (", enabled: ").append (enabled)
			.append (", connected: ").append (connected)
			.append (")");
		if (busy && entryNumber > 0) {
			sb.append (", entryNumber: ").append (entryNumber);
		}
		if (jobStart != null) {
			sb.append (", job started: ").append (jobStart);
		}
		if (jobStop != null) {
			sb.append (", job stoped: ").append (jobStop);
		}
		return sb.toString ();
	}
	
	public JsonObject toJson ()
	{
		JsonObject json = new JsonObject ();
		json.put ("remotePath", remotePath)
			.put ("displayName", displayName)
			.put ("distTransferPath", distTransferPath)
			.put ("distExe", distExe)
			.put ("type", type)
			.put ("host", host)
			.put ("job", job)
			.put ("enabled", enabled)
			.put ("connected", connected)
			.put ("offline", offline)
			.put ("busy", busy)
			.put ("starting", starting)
			.put ("toBeStopped", toBeStopped)
			.put ("port", port)
			.put ("jobNumber", jobNumber)
			.put ("protocol", protocol)
			.put ("hostType", hostType)
			.put ("jobStart", (jobStart != null ? jobStart.getTime () : -1L))
			.put ("jobStop", (jobStop != null ? jobStart.getTime () : -1L))
			.put ("failedToStart", failedToStart);
		return json;
	}
	
	private String stringValue;
	private String getStringValue ()
	{
		if (stringValue == null) {
			stringValue = displayName + "#" + type + "#" + host + "#" + port;
		}
		return stringValue;
	}

	@Override
	public boolean equals (Object obj)
	{
		if (obj instanceof WorkerInfo) {
			return getStringValue ().equals (((WorkerInfo) obj).getStringValue ());
		}
		return super.equals (obj);
	}

	@Override
	public int hashCode ()
	{
		return getStringValue ().hashCode ();
	}
	
}
