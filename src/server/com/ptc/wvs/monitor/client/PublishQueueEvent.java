package com.ptc.wvs.monitor.client;

import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;

import com.sm.javax.langx.Strings;

public abstract class PublishQueueEvent
{
	public static final PublishQueueEvent JobFailedEvent = new QueueEntryEvent ("JobFailedEvent", 
			qe -> qe.isFailed ());
	
	public static final PublishQueueEvent JobSuccededEvent = new QueueEntryEvent ("JobSuccededEvent",
			qe -> qe.isSucceded ());
	
	public static final PublishQueueEvent JobFinishedEvent = new QueueEntryEvent ("JobFinishedEvent",
			qe -> qe.isFinished ());
	
	public static final PublishQueueEvent NewJobEvent = new DataSourceEvent ("NewJobEvent", 
			ds -> {
				List<QueueEntryInfo> list = ds.getQueueEntriesWithStatus (StatusInfoCode.READY);
				return (list != null && !list.isEmpty ());
			});
	
	private String eventName;
	
	private PublishQueueEvent (String eventName)
	{
		super ();
		this.eventName = eventName;
	}
	
	public static PublishQueueEvent createDataSourceEvent (String eventName, Predicate<PublishQueuesDataSource> condition)
	{
		return new DataSourceEvent (eventName, condition);
	}
	
	public static PublishQueueEvent createQueueEntryEvent (String eventName, Predicate<QueueEntryInfo> condition)
	{
		return new QueueEntryEvent (eventName, condition);
	}
	
	public static PublishQueueEvent createWorkerEvent (String eventName, Predicate<WorkerInfo> condition)
	{
		return new WorkerEvent (eventName, condition);
	}
	
	public abstract boolean test (Object obj);
	
	@Override
	public String toString ()
	{
		return eventName;
	}
	
	// ----------------------------------------------------------------------
	private static class DataSourceEvent extends PublishQueueEvent
	{
		private Predicate<PublishQueuesDataSource> condition;

		public DataSourceEvent (String eventName, Predicate<PublishQueuesDataSource> condition)
		{
			super (eventName);
			this.condition = condition;
		}
		
		@Override
		public boolean test (Object obj)
		{
			if (obj instanceof PublishQueuesDataSource && condition != null) {
				return condition.test ((PublishQueuesDataSource) obj);
			}
			return false;
		}
		
	}
	
	// ----------------------------------------------------------------------
	private static class QueueEntryEvent extends PublishQueueEvent
	{
		private Predicate<QueueEntryInfo> condition;

		public QueueEntryEvent (String eventName, Predicate<QueueEntryInfo> condition)
		{
			super (eventName);
			this.condition = condition;
		}
		
		@Override
		public boolean test (Object obj)
		{
			if (obj instanceof QueueEntryInfo && condition != null) {
				return condition.test ((QueueEntryInfo) obj);
			}
			return false;
		}
		
	}
	
	// ----------------------------------------------------------------------
	private static class WorkerEvent extends PublishQueueEvent
	{
		private Predicate<WorkerInfo> condition;
		
		public WorkerEvent (String eventName, Predicate<WorkerInfo> condition)
		{
			super (eventName);
			this.condition = condition;
		}
		
		@Override
		public boolean test (Object obj)
		{
			if (obj instanceof WorkerInfo && condition != null) {
				return condition.test ((WorkerInfo) obj);
			}
			else if (obj instanceof List<?> && condition != null) {
				for (Object o : (List<?>) obj) {
					if (test (o)) {
						return true;
					}
				}
			} 
			return false;
		}
		
	}
	
	// ----------------------------------------------------------------------
	public static PublishQueueEvent getQueueIsEmptyEvent (String queueName)
	{
		PublishQueueEvent event = new DataSourceEvent ("Queue-" + queueName + "-EmptyEvent", 
				ds -> { 
					List<QueueEntryInfo> queue = ds.getQueue (queueName);
					return (queue == null || queue.isEmpty ());
				}
			);
		return event;
	}
	
	public static PublishQueueEvent getQueueIsEmptyEvent (String queueName, String ...otherQueueNames)
	{
		String[] queueNames = combine (queueName, otherQueueNames);
		String eventId = Strings.concat (queueNames, "-");
		PublishQueueEvent event = new DataSourceEvent ("Queue-" + eventId + "-EmptyEvent", 
				ds -> { 
					HashMap<QueueInfo, List<QueueEntryInfo>> queues = ds.getQueues (queueNames);
					if (queues != null) {
						for (List<QueueEntryInfo> queue : queues.values ()) { 
							if (queue != null && !queue.isEmpty ()) {
								return false;
							}
						}
					}
					return true;
				}
			);
		return event;
	}
	
	public static PublishQueueEvent getQueueIsNotEmptyEvent (String queueName)
	{
		PublishQueueEvent event = new DataSourceEvent ("Queue-" + queueName + "-NotEmptyEvent",
				ds -> { 
					List<QueueEntryInfo> queue = ds.getQueue (queueName);
					return (queue != null && !queue.isEmpty ());
				}
			);
		return event;
	}
	
	private static String[] combine (String a, String arr[])
	{
		int size = (arr != null ? arr.length : 0);
		String res[] = new String[size+1] ;
		res [0] = a;
		for (int i=0; i<size; i++) {
			res [i+1] = arr [i];
		}
		return res;
	}
	
	public static PublishQueueEvent getQueueIsNotEmptyEvent (String queueName, String ...otherQueueNames)
	{
		String[] queueNames = combine (queueName, otherQueueNames);
		String eventId = Strings.concat (queueNames, "-");
		PublishQueueEvent event = new DataSourceEvent ("Queue-" + eventId + "-NotEmptyEvent",
				ds -> { 
					HashMap<QueueInfo, List<QueueEntryInfo>> queues = ds.getQueues (queueNames);
					if (queues != null) {
						for (List<QueueEntryInfo> queue : queues.values ()) { 
							if (queue != null && !queue.isEmpty ()) {
								return true;
							}
						}
					}
					return false;
				}
			);
		return event;
	}
	
	public static final PublishQueueEvent getJobRecentlySuccededEvent (long timePeriorMsec) 
	{
		Predicate<QueueEntryInfo> filter = PublishQueuesDataSource.getRecentlyCompletedJobFilter (timePeriorMsec);
		PublishQueueEvent event = new DataSourceEvent ("JobRecently-"+timePeriorMsec+"-SuccededEvent",
				ds -> {
					List<QueueEntryInfo> list = ds.getQueueEntries (filter);
					return (list != null && !list.isEmpty ());
				}
			);
		return event;
	}
	
	public static final PublishQueueEvent getJobRecentlyFailedEvent (long timePeriorMsec) 
	{
		Predicate<QueueEntryInfo> filter = PublishQueuesDataSource.getRecentlyFailedJobFilter (timePeriorMsec);
		PublishQueueEvent event = new DataSourceEvent ("JobRecently-"+timePeriorMsec+"-FailedEvent",
				ds -> {
					List<QueueEntryInfo> list = ds.getQueueEntries (filter);
					return (list != null && !list.isEmpty ());
				}
			);
		return event;
	}
	
	public static final PublishQueueEvent getJobRecentlyFinishedEvent (long timePeriorMsec) 
	{
		Predicate<QueueEntryInfo> filter = PublishQueuesDataSource.getRecentlyFinishedJobFilter (timePeriorMsec);
		PublishQueueEvent event = new DataSourceEvent ("JobRecently-"+timePeriorMsec+"-FinishedEvent",
				ds -> {
					List<QueueEntryInfo> list = ds.getQueueEntries (filter);
					return (list != null && !list.isEmpty ());
				}
			);
		return event;
	}
	
	public static final PublishQueueEvent getWorkerBecameBusyEvent (String workerName)
	{
		Predicate<WorkerInfo> filter = PublishQueuesDataSource.getIsBusyWorkerFilter (workerName);
		PublishQueueEvent event = PublishQueueEvent.createWorkerEvent ("Worker-"+workerName+"-busy", filter);
		return event;
	}
	
	public static final PublishQueueEvent getWorkerBecameFreeEvent (String workerName)
	{
		PublishQueueEvent event = PublishQueueEvent.createWorkerEvent ("Worker-"+workerName+"-free", 
				w -> (w != null && w.getDisplayName ().equals (workerName) && !w.isBusy ()));
		return event;
	}

	// ----------------------------------------------------------------------
	public static interface EventNotifier 
	{
		public void notify (PublishQueueEvent event);
	}
	
}
