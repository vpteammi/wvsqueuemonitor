package com.ptc.wvs.monitor.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WorkersList implements Collection<WorkerInfo>
{
	
	private List<WorkerInfo>        workers;
	private Map<String, WorkerInfo> workersById;
	
	public WorkersList (List<WorkerInfo> list)
	{
		super ();
		workers = new LinkedList<WorkerInfo> ();
		workersById = new HashMap<String, WorkerInfo> ();
		if (list != null) {
			list.forEach (w -> addWorker (w));
		}
	}
	
	public WorkersList ()
	{
		this (new LinkedList<WorkerInfo> ());
	}
	
	// --------------------------------------------------------------
	public synchronized boolean addWorker (WorkerInfo worker)
	{
		String name = worker.getDisplayName ();
		WorkerInfo oldWorker = workersById.get (name);
		if (oldWorker != null) {
			workers.remove (oldWorker);
		}
		workers.add (worker);
		workersById.put (name, worker);
		return true;
	}
	
	public synchronized boolean removeWorker (WorkerInfo worker)
	{
		String name = worker.getDisplayName ();
		WorkerInfo oldWorker = workersById.get (name);
		if (oldWorker != null) {
			workers.remove (oldWorker);
			workersById.remove (name);
			return true;
		}
		return false;
	}
	
	public synchronized void setWorkers (List<WorkerInfo> list)
	{
		clear ();
		if (list != null) {
			list.forEach (w -> addWorker (w));
		}
	}
	
	public synchronized void setWorker (WorkerInfo worker)
	{
		addWorker (worker);
	}
	
	public synchronized WorkerInfo getWorkerByName (String displayName)
	{
		return workersById.get (displayName);
	}
	
	public synchronized List<WorkerInfo> getBusyWorkers ()
	{
		return workers.stream ().filter (w -> w.isBusy ()).collect (Collectors.toList ());
	}

	// --------------------------------------------------------------
	@Override
	public boolean add (WorkerInfo worker)
	{
		return addWorker (worker);
	}

	@Override
	public boolean addAll (Collection<? extends WorkerInfo> list)
	{
		if (list != null) {
			list.forEach (w -> addWorker (w));
		}
		return true;
	}

	@Override
	public void clear ()
	{
		workers.clear ();
		workersById.clear ();
	}

	@Override
	public boolean contains (Object obj)
	{
		return workers.contains (obj);
	}

	@Override
	public boolean containsAll (Collection<?> list)
	{
		return workers.containsAll (list);
	}

	@Override
	public boolean isEmpty ()
	{
		return workers.isEmpty ();
	}

	@Override
	public Iterator<WorkerInfo> iterator ()
	{
		return workers.iterator ();
	}

	@Override
	public boolean remove (Object obj)
	{
		if (obj instanceof WorkerInfo) {
			return removeWorker ((WorkerInfo) obj);
		}
		return false;
	}

	@Override
	public boolean removeAll (Collection<?> list)
	{
		boolean res = false;
		if (list != null) {
			for (Object elem : list) {
				res = res || remove (elem);
			}
		}
		return res;
	}

	@Override
	public boolean retainAll (Collection<?> list)
	{
		boolean res = false;
		if (list != null) {
			for (WorkerInfo worker : workers) {
				if (! list.contains (worker)) {
					res = res || removeWorker (worker);
				}
			}
		}
		else {
			return workers.removeAll (workers);
		}
		return res;
	}

	@Override
	public int size ()
	{
		return workers.size ();
	}

	@Override
	public Object[] toArray ()
	{
		return workers.toArray ();
	}

	@Override
	public <T> T[] toArray (T[] arr)
	{
		return workers.toArray (arr);
	}

}
