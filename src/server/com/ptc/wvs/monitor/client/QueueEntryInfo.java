package com.ptc.wvs.monitor.client;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.vertx.core.json.JsonObject;

public class QueueEntryInfo
{
	public static final SimpleDateFormat DateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");

	// 2018-02-26T13:16:20Z
	private static SimpleDateFormat RestDateFormat = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ss'Z'");
	// 2018-03-15 12:34:56.468 or 2018-03-15 12:34:56.4
	private static SimpleDateFormat BasicDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.S");
	
	public static final String[] WCAttributes = {"queueRef", "queue.name", "statusInfo.code", "startExec", 
			"endExec", "failureCount", "entryOwner.name", "entryOwner.fullName", 
			"args[0].arg.persistableRef", "args[0].arg.jobType", "entryNumber"};
	
	private String              id;
	private Date                startExec;
	private Date                endExec;
	private StatusInfoCode      statusInfoCode;
	private String              queueRef;
	private QueueInfo           queue;
	private String              entryOwnerName;
	private String              entryOwnerFullName;
	private int                 failureCount;
	private String              jobType;
	private int                 jobNumber;
	private EpmAuthoringAppType	authAppType;
	private String              persistableRef;
	private EpmDocumentInfo     docInfo;
	private WindchillInfoReader wcReader;
	
	public QueueEntryInfo (JsonObject json, WindchillInfoReader wcReader)
	{
		super ();
		this.wcReader = wcReader;
		if (json.containsKey ("id")) {
			id = json.getString ("id");
		}
		if (json.containsKey ("attributes")) {
			JsonObject attrs = json.getJsonObject ("attributes");
			statusInfoCode = StatusInfoCode.create (attrs.containsKey ("statusInfo.code") ? attrs.getString ("statusInfo.code") : null);

			queueRef = (attrs.containsKey ("queueRef") ? attrs.getString ("queueRef") : null);
			queue = QueueInfo.createByRef (queueRef);
			/*
			String queueName = (attrs.containsKey ("queue.name") ? attrs.getString ("queue.name") : null);
			queue = QueueInfo.create (queueName, queueRef);
			*/

			entryOwnerName = (attrs.containsKey ("entryOwner.name") ? attrs.getString ("entryOwner.name") : null);
			entryOwnerFullName = (attrs.containsKey ("entryOwner.fullName") ? attrs.getString ("entryOwner.fullName") : null);
			failureCount = attrs.getInteger ("failureCount", 0);
			startExec = getDate (attrs, "startExec");
			endExec = getDate (attrs, "endExec");
			persistableRef = (attrs.containsKey ("args[0].arg.persistableRef") ? attrs.getString ("args[0].arg.persistableRef") : null);
			if (persistableRef != null) {
				docInfo = EpmDocumentInfo.find (persistableRef);
			}
			jobType = (attrs.containsKey ("args[0].arg.jobType") ? attrs.getString ("args[0].arg.jobType") : null);
			jobNumber = (attrs.containsKey ("entryNumber") ? attrs.getInteger ("entryNumber") : -1);
			String authApp = (attrs.containsKey ("authApp") ? attrs.getString ("authApp") : null);
			if (authApp == null && docInfo != null) {
				authApp = docInfo.getAuthoringApplication ();
			}
			authAppType = EpmAuthoringAppType.create (authApp);
		}
	}

	public QueueEntryInfo (JsonObject json)
	{
		this (json, null);
	}
	
	public void setWindchillInfoReader (WindchillInfoReader wcReader)
	{
		this.wcReader = wcReader;
	}
	
	private Date getDate (JsonObject attrs, String attrname)
	{
		Date res = null;
		if (attrs.containsKey (attrname)) {
			String dateStr = attrs.getString (attrname);
			try {
				res = RestDateFormat.parse (dateStr);
			}
			catch (ParseException e) {
				try {
					res = BasicDateFormat.parse (dateStr);
				}
				catch (ParseException e1) {
					System.err.println ("Failed to convert '" + dateStr + "' to date - " + e.getMessage ());
				}
			}
		}
		return res;
	}

	public String getId ()
	{
		return id;
	}

	public Date getStartExec ()
	{
		return startExec;
	}

	public Date getEndExec ()
	{
		return endExec;
	}

	public StatusInfoCode getStatusInfoCode ()
	{
		return statusInfoCode;
	}

	public QueueInfo getQueue ()
	{
		if (queue == null && queueRef != null) {
			queue = QueueInfo.createByRef (queueRef); 
		}
		return queue;
	}

	public String getEntryOwnerName ()
	{
		return entryOwnerName;
	}

	public String getEntryOwnerFullName ()
	{
		return entryOwnerFullName;
	}

	public int getFailureCount ()
	{
		return failureCount;
	}
	
	public String getJobType ()
	{
		return jobType;
	}
	
	public int getJobNumber ()
	{
		return jobNumber;
	}

	public EpmAuthoringAppType getAuthApp ()
	{
		if (authAppType == null) { 
			if (docInfo == null && persistableRef != null) {
				docInfo = EpmDocumentInfo.find (persistableRef);
				if (docInfo != null) {
					authAppType = EpmAuthoringAppType.create (docInfo.getAuthoringApplication ());
				}
			}
		}
		return authAppType;
	}

	public String getPersistableRef ()
	{
		return persistableRef;
	}
	
	public boolean hasDocumentInfo ()
	{
		return (docInfo != null);
	}
	
	public EpmDocumentInfo getDocumentInfo ()
	{
		if (docInfo == null && persistableRef != null) {
			docInfo = EpmDocumentInfo.find (persistableRef);
			if(docInfo == null && wcReader != null) {
				docInfo = wcReader.getDocumentInfo (persistableRef);
			}
		}
		return docInfo;
	}
	
	public void setDocumentInfo (EpmDocumentInfo docInfo)
	{
		this.docInfo = docInfo;
	}

	@Override
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("id: ").append (id);
		sb.append (", status: ").append (statusInfoCode != null ? statusInfoCode.toString () : "null")
			.append (", queue: ").append (getQueue ())
			.append (", startExec: ").append (startExec)
			.append (", endExec: ").append (endExec)
			.append (", failureCount: ").append (failureCount)
			.append (", owner: ").append (entryOwnerName)
			.append (", persistableRef: ").append (persistableRef);
		if (authAppType != null) {
			sb.append (", authApp: ").append (authAppType.toString ());
		}
		if (jobNumber > -1) {
			sb.append ("jobNumber").append (jobNumber);
		}
		return sb.toString ();
	}
	
	public JsonObject toJson ()
	{
		JsonObject json = new JsonObject ();
		StatusInfoCode statusInfo = getStatusInfoCode ();
		json.put ("id", getId ())
			.put ("status", (statusInfo != null ? statusInfo.getValue () : null))
			.put ("queueRef", queueRef)
			.put ("documentRef", getPersistableRef ())
			.put ("jobType", getJobType ())
			.put ("jobNumber", getJobNumber ())
			.put ("startExec", dateToString (getStartExec ()))
			.put ("endExec", dateToString (getEndExec ()));
		EpmAuthoringAppType authApp = getAuthApp ();
		if (authApp != null) {
			json.put ("authApp", authApp.toString ());
		}
		if (hasDocumentInfo ()) {
			EpmDocumentInfo doc = getDocumentInfo ();
			json.put ("docInfo", doc!=null?doc.toJson ():null);
		}
		return json;
	}
	
	public boolean isAfter (Date afterTime)
	{
		if (afterTime == null) {
			return true;
		} 
		Date end = getEndExec ();
		long at = afterTime.getTime ();
		if (end != null) {
			return (end.getTime () > at);
		}
		Date start = getStartExec ();
		if (start != null) {
			return (start.getTime () > at);
		}
		return true;
	}

	public boolean isBefore (Date beforeTime)
	{
		if (beforeTime == null) {
			return true;
		} 
		Date end = getEndExec ();
		long bt = beforeTime.getTime ();
		if (end != null) {
			return (end.getTime () < bt);
		}
		Date start = getStartExec ();
		if (start != null) {
			return (start.getTime () < bt);
		}
		return true;
	}

	public boolean isFailed ()
	{
		return (StatusInfoCode.FAILED == statusInfoCode || StatusInfoCode.NONEVENTFAILED == statusInfoCode);
	}
	
	public boolean isSucceded ()
	{
		return (StatusInfoCode.FAILED == statusInfoCode);
	}
	
	public boolean isFinished ()
	{
		return (endExec != null && statusInfoCode != null);
	}

	// ---------------------------------------------------------------------------------------------
	private static String dateToString (Date d)
	{
		if (d != null) {
			return DateFormat.format (d);
		}
		return null;
	}

}
