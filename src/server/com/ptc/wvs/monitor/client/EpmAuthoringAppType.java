package com.ptc.wvs.monitor.client;

import java.util.HashMap;

import io.vertx.core.json.JsonObject;

public class EpmAuthoringAppType
{
	private static HashMap<String, EpmAuthoringAppType> Types = new HashMap<> ();
	
	public static final EpmAuthoringAppType PROE = new EpmAuthoringAppType ("PROE", "Creo");
	public static final EpmAuthoringAppType GALAXY = new EpmAuthoringAppType ("GALAXY", "Creo Illustrate");
	public static final EpmAuthoringAppType ECADNEUTRAL = new EpmAuthoringAppType ("ECADNEUTRAL", "ECAD Neutral");
	public static final EpmAuthoringAppType INVENTOR = new EpmAuthoringAppType ("INVENTOR", "Inventor");
	public static final EpmAuthoringAppType SOLIDWORKS = new EpmAuthoringAppType ("SOLIDWORKS", "SolidWorks");
	public static final EpmAuthoringAppType CATIAV5 = new EpmAuthoringAppType ("CATIAV5", "CATIA V5");
	
	public static final EpmAuthoringAppType CADENCE = new EpmAuthoringAppType ("CADENCE", "Cadence Allegro PCB Editor");
	public static final EpmAuthoringAppType CADENCE_SCH = new EpmAuthoringAppType ("CADENCE_SCH", "Cadence Allegro Design Entry HDL");
	public static final EpmAuthoringAppType CADENCE_APD = new EpmAuthoringAppType ("CADENCE_APD", "Cadence Allegro Advanced Package Designer");
	public static final EpmAuthoringAppType ORCAD_SCH = new EpmAuthoringAppType ("ORCAD_SCH", "Cadence Allegro Design Entry CIS/OrCAD Capture");

	public static final EpmAuthoringAppType MENTORD = new EpmAuthoringAppType ("MENTORD", "Mentor Graphics DxDesigner");
	public static final EpmAuthoringAppType MENTORE_SCH = new EpmAuthoringAppType ("MENTORE_SCH", "Mentor Graphics Design Capture");
	public static final EpmAuthoringAppType MENTORE = new EpmAuthoringAppType ("MENTORE", "Mentor Graphics Expedition PCB");
	public static final EpmAuthoringAppType PADS = new EpmAuthoringAppType ("PADS", "Mentor Graphics PADS Layout");
	public static final EpmAuthoringAppType PADS_SCH = new EpmAuthoringAppType ("PADS_SCH", "Mentor Graphics PADS Logic");
	
	public static final EpmAuthoringAppType ZUKENCR5_SCH = new EpmAuthoringAppType ("ZUKENCR5_SCH", "Zuken CR5000 System Designer");
	public static final EpmAuthoringAppType ZUKENCR8DGW_SCH = new EpmAuthoringAppType ("ZUKENCR8DGW_SCH", "Zuken CR-8000 Design Gateway");
	public static final EpmAuthoringAppType ZUKENCR5 = new EpmAuthoringAppType ("ZUKENCR5", "Zuken CR5000 Board Designer");
	public static final EpmAuthoringAppType ZUKENCR8DF = new EpmAuthoringAppType ("ZUKENCR8DF", "Zuken CR-8000 Design Force");
	
	public static final EpmAuthoringAppType PROTEL = new EpmAuthoringAppType ("PROTEL", "Altium Designer PCB");
	public static final EpmAuthoringAppType PROTEL_SCH = new EpmAuthoringAppType ("PROTEL_SCH", "Altium Designer Schematic");

	public static final EpmAuthoringAppType UG = new EpmAuthoringAppType ("UG", "NX");

	// ---------------------------------------------------------------------------
	private String value;
	private String description;
	
	private EpmAuthoringAppType (String value, String description)
	{
		super ();
		this.value = value;
		this.description = description;
		Types.put (description, this);
	}
	
	public static EpmAuthoringAppType create (String description)
	{
		if (description == null) {
			return null;
		}
		EpmAuthoringAppType type = Types.get (description);
		if (type == null) {
			String v = description.replace (" ", "_").toUpperCase ();
			type = new EpmAuthoringAppType ("UNKNOWN-" + v, description);
		}
		return type;
	}

	public static EpmAuthoringAppType create (JsonObject json)
	{
		String description = json.getString ("display");
		String value = json.getString ("key");
		EpmAuthoringAppType type = Types.get (description);
		if (type == null || ! type.getValue ().equals (value)) {
			type = new EpmAuthoringAppType (value, description);
		}
		return type;
	}

	public static EpmAuthoringAppType findByValue (String value)
	{
		EpmAuthoringAppType type = null;
		if (value != null) {
			for (EpmAuthoringAppType val : Types.values ()) {
				if (value.equalsIgnoreCase (val.getValue ())) {
					type = val;
					break;
				}
			}
		}
		return type;
	}
	
	public String getValue ()
	{
		return value;
	}

	public String getDescription ()
	{
		return description;
	}
	
	@Override
	public String toString ()
	{
		return value;
	}

}
