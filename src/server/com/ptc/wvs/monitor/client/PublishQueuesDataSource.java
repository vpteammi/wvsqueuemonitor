package com.ptc.wvs.monitor.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface PublishQueuesDataSource
{
	
	HashMap<QueueInfo, List<QueueEntryInfo>> getQueues ();

	HashMap<QueueInfo, List<QueueEntryInfo>> getQueues (QueueInfo... queueInfos);

	HashMap<QueueInfo, List<QueueEntryInfo>> getQueues (String... queueNames);

	List<QueueEntryInfo> getQueue (QueueInfo queueInfo);

	List<QueueEntryInfo> getQueue (String queueName);
	
	EpmDocumentInfo getDocumentInfo (String docId);

	void getDocumentInfo (String docId, Consumer<EpmDocumentInfo> successHandler, Consumer<Throwable> errHandler);
	
	// -------------------
	void registerEventListener (PublishQueueEvent event, PublishQueueEvent.EventNotifier listener);
	
	// -------------------
	PublishQueuesDataSource timeout (long timeoutMsec);
	
	PublishQueuesDataSource waitRequestTimeout (long timeoutMsec);
	
	PublishQueuesDataSource errorResendTimeout (long timeoutMsec);
	
	PublishQueuesDataSource maxConsecutiveErrorCount (int maxConsecutiveErrCount);

	long getTimeout ();
	long getWaitRequestTimeout ();
	long getErrorResendTimeout ();
	int getMaxConsecutiveErrorCount ();
	
	// -------------------
	List<WorkerInfo> getWorkers ();

	List<WorkerInfo> getBusyWorkers ();
	
	WorkerInfo getWorker (String displayName);
	
	// -------------------
	List<QueueEntryInfo> getQueueEntries (Predicate<QueueEntryInfo> filter);

	default List<QueueEntryInfo> getQueueEntriesWithStatus (StatusInfoCode status) {
		return getQueueEntries (q -> q.getStatusInfoCode () == status);
	}
	
	default List<QueueEntryInfo> getQueueEntriesWithAuthApp (EpmAuthoringAppType appType) {
		return getQueueEntries (q -> q.getAuthApp () == appType);
	}
	
	static long compare (Date d, Date end, Date start)
	{
		if (d != null) {
			if (end != null) {
				return (d.getTime () - end.getTime ());
			}
			if (start != null) {
				return (d.getTime () - start.getTime ());
			}
		}
		return 0;
	}
	
	default List<QueueEntryInfo> getQueueEntriesExecutedAfter (Date after) {
		Predicate<QueueEntryInfo> filter = new Predicate<QueueEntryInfo> () {
			@Override
			public boolean test (QueueEntryInfo q)
			{
				if (after != null) {
					Date end = q.getEndExec ();
					if (end != null) {
						return (end.getTime () > after.getTime ());
					}
					Date start = q.getStartExec ();
					if (start != null) {
						return (start.getTime () > after.getTime ());
					}
				}
				return true;
			}
		};
		return getQueueEntries (filter);
	}
	
	default List<QueueEntryInfo> getQueueEntriesExecutedBefore (Date before) {
		Predicate<QueueEntryInfo> filter = new Predicate<QueueEntryInfo> () {
			@Override
			public boolean test (QueueEntryInfo q)
			{
				if (before != null) {
					Date end = q.getEndExec ();
					if (end != null) {
						return (end.getTime () < before.getTime ());
					}
					Date start = q.getStartExec ();
					if (start != null) {
						return (start.getTime () < before.getTime ());
					}
				}
				return true;
			}
		};
		return getQueueEntries (filter);
	}
	
	public static Predicate<QueueEntryInfo> getRecentlyCompletedJobFilter (long msec) 
	{
		Predicate<QueueEntryInfo> filter = qe -> {
			Date endTime = qe.getEndExec (); 
			if (endTime != null && (new Date ().getTime () - endTime.getTime ()) < msec) {
				return qe.getStatusInfoCode () == StatusInfoCode.COMPLETED;
			}
			return false;
		};
		return filter;
	}
		
	public static Predicate<QueueEntryInfo> getRecentlyFailedJobFilter (long msec) 
	{
		Predicate<QueueEntryInfo> filter = qe -> {
			Date endTime = qe.getEndExec (); 
			if (endTime != null && (new Date ().getTime () - endTime.getTime ()) < msec) {
				StatusInfoCode code = qe.getStatusInfoCode ();
				return (code == StatusInfoCode.FAILED || code == StatusInfoCode.NONEVENTFAILED);
			}
			return false;
		};
		return filter;
	}
		
	public static Predicate<QueueEntryInfo> getRecentlyFinishedJobFilter (long msec) 
	{
		Predicate<QueueEntryInfo> filter = qe -> {
			Date endTime = qe.getEndExec (); 
			if (endTime != null && (new Date ().getTime () - endTime.getTime ()) < msec) {
				StatusInfoCode code = qe.getStatusInfoCode ();
				return (code == StatusInfoCode.FAILED || code == StatusInfoCode.NONEVENTFAILED || code == StatusInfoCode.COMPLETED);
			}
			return false;
		};
		return filter;
	}
		
	public static Predicate<WorkerInfo> getIsBusyWorkerFilter (String displayName)
	{
		Predicate<WorkerInfo> filter = w -> {
			return (w != null && displayName.equals (w.getDisplayName ()) && w.isBusy ());
		};
		return filter;
	}
}
