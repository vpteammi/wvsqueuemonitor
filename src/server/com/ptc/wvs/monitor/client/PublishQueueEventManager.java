package com.ptc.wvs.monitor.client;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.ptc.wvs.monitor.client.PublishQueueEvent.EventNotifier;

public class PublishQueueEventManager
{
	
	private PublishQueuesDataSource    dataSource;
	private HashMap<PublishQueueEvent, List<PublishQueueEvent.EventNotifier>> regEvents;
	
	public PublishQueueEventManager (PublishQueuesDataSource dataSource)
	{
		super ();
		this.dataSource = dataSource;
		this.regEvents = new HashMap<> (); 
	}
	
	public void registerListener (PublishQueueEvent event, PublishQueueEvent.EventNotifier listener)
	{
		List<EventNotifier> listeners = regEvents.get (event);
		if (listeners == null) {
			listeners = new LinkedList<> ();
			regEvents.put (event, listeners);
		}
		listeners.add (listener);
	}
	
	public void invokeEvent (PublishQueueEvent event)
	{
		List<EventNotifier> listeners = regEvents.get (event);
		if (listeners != null) {
			for (EventNotifier listener : listeners) {
				listener.notify (event);
			}
		}
	}
	
	public void dataUpdated ()
	{
		for (PublishQueueEvent event : regEvents.keySet ()) {
			if (event.test (dataSource)) {
				invokeEvent (event);
			}
		}
	}

	public void workersUpdated ()
	{
		for (PublishQueueEvent event : regEvents.keySet ()) {
			if (event.test (dataSource.getWorkers ())) {
				invokeEvent (event);
			}
		}
	}

	public void dataUpdated (QueueEntryInfo qentry)
	{
		for (PublishQueueEvent event : regEvents.keySet ()) {
			if (event.test (qentry)) {
				invokeEvent (event);
			}
		}
	}

	public void dataUpdated (WorkerInfo worker)
	{
		for (PublishQueueEvent event : regEvents.keySet ()) {
			if (event.test (worker)) {
				invokeEvent (event);
			}
		}
	}
	
}
