package com.ptc.wvs.monitor.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.sm.javax.langx.annotation.SettingProperty;
import com.sm.javax.utilx.ApplicationProperties;

import io.vertx.core.json.JsonObject;

public class PublishQueueMonitor implements PublishQueuesDataSource
{
	@SettingProperty (defaultValue="90000", documentation="Timeout between publish queues requests sent to Windchill (msec)")
	private static long DefaultRequestTimeout;
	
	@SettingProperty (defaultValue="10000", documentation="Wait timeout for sending requst to Windchill upon receiving "
			+ "statuc info change notification (msec)")
	private static long DefaultWaitTimeout;
	
	@SettingProperty (defaultValue="5000", documentation="Timeout for resending failed request (msec)")
	private static long DefaultErrorResendTimeout;
	
	@SettingProperty (defaultValue="5", documentation="Max consecutive error count before terminating monitor")
	private static int DefaultMaxConsecutiveErrCount;
	
	@SettingProperty (defaultValue="false", cmdLineName="verbose", documentation="Debug output ON/OFF")
	private static boolean QueuesDebugOutput;
	
	@SettingProperty (defaultValue="true", documentation="Retrieve info about EPM documents for publish queue entries right after "
			+ "receiving queue entries without waiting requests for them")
	private static boolean RequestEpmDocuments;
	
	@SettingProperty (defaultValue="false", documentation="Print debug info about retrieved EPM document info")
	private static boolean DocumentsDebugOutput;
	
	static {
		ApplicationProperties.processStaticProperties (PublishQueueMonitor.class);
	}
	
	// ====================================================================================================
	private ScheduledThreadPoolExecutor              executor;
	private WindchillInfoReader                      wcReader;
	private boolean                                  keepWorking; 
	private HashMap<QueueInfo, List<QueueEntryInfo>> queues;
	private WorkersList                              workers;
	private long                                     timeout;
	private long                                     waitTimeout;
	private long                                     errorResendTimeout;
	private long                                     nextRequestTime;
	private ScheduledFuture<?>                       scheduledRequest;
	private PublishQueueEventManager                 eventManager;
	
	private int                                      maxConsecutiveErrCount;
	private int                                      consecutiveQueueErrCount;
	private int                                      consecutiveWorkersErrCount;
	private int                                      queueListErrCount;

	private boolean                                  requestWorkers;
	
	public PublishQueueMonitor (WindchillInfoReader wcReader)
	{
		super ();
		this.wcReader = wcReader;
		this.executor = new ScheduledThreadPoolExecutor (2);
		this.timeout = DefaultRequestTimeout;
		this.waitTimeout = DefaultWaitTimeout;
		this.errorResendTimeout = DefaultErrorResendTimeout;
		this.consecutiveQueueErrCount = 0;
		this.consecutiveWorkersErrCount = 0;
		this.requestWorkers = true;
		this.queueListErrCount = 0;
		this.maxConsecutiveErrCount = DefaultMaxConsecutiveErrCount;
		this.keepWorking = true;
		this.workers = new WorkersList (); 
	}
	
	@Override
	public void registerEventListener (PublishQueueEvent event, PublishQueueEvent.EventNotifier listener)
	{
		if (eventManager == null) {
			eventManager = new PublishQueueEventManager (this);
		}
		eventManager.registerListener (event, listener);
	}
	
	@Override
	public PublishQueueMonitor timeout (long timeoutMsec)
	{
		if (timeoutMsec > 0) {
			this.timeout = timeoutMsec;
		}
		return this;
	}
	
	@Override
	public PublishQueueMonitor waitRequestTimeout (long timeoutMsec)
	{
		if (timeoutMsec > 0) {
			this.waitTimeout = timeoutMsec;
		}
		return this;
	}
	
	@Override
	public PublishQueueMonitor errorResendTimeout (long timeoutMsec)
	{
		if (timeoutMsec > 0) {
			this.errorResendTimeout = timeoutMsec;
		}
		return this;
	}
	
	@Override
	public PublishQueueMonitor maxConsecutiveErrorCount (int maxConsecutiveErrCount)
	{
		if (maxConsecutiveErrCount > 0) {
			this.maxConsecutiveErrCount = maxConsecutiveErrCount;
		}
		return this;
	}
	
	@Override
	public long getTimeout () { return timeout; }
	
	@Override
	public long getWaitRequestTimeout () { return waitTimeout; }
	
	@Override
	public long getErrorResendTimeout () { return errorResendTimeout; }
	
	@Override
	public int getMaxConsecutiveErrorCount () { return maxConsecutiveErrCount; }


	@Override
	public synchronized HashMap<QueueInfo, List<QueueEntryInfo>> getQueues ()
	{
		return queues;
	}
	
	@Override
	public synchronized HashMap<QueueInfo, List<QueueEntryInfo>> getQueues (QueueInfo ...queueInfos)
	{
		HashMap<QueueInfo, List<QueueEntryInfo>> resQueues = null;
		if (queues != null) {
			resQueues = new HashMap<> (queues.size ());
			for (QueueInfo queueInfo : queueInfos) {
				List<QueueEntryInfo> queuesList = queues.get (queueInfo);
				if (queuesList != null) {
					resQueues.put (queueInfo, queuesList);
				}
			}
		}
		return resQueues;
	}
	
	@Override
	public HashMap<QueueInfo, List<QueueEntryInfo>> getQueues (String ...queueNames)
	{
		return getQueues (QueueInfo.getQueuesByName (queueNames));
	}
	
	@Override
	public synchronized List<QueueEntryInfo> getQueue (QueueInfo queueInfo)
	{
		if (queues != null && queueInfo != null) {
			return queues.get (queueInfo);
		}
		return null;
	}
	
	@Override
	public List<QueueEntryInfo> getQueue (String queueName)
	{
		if (queues != null) {
			QueueInfo queue = QueueInfo.getQueueByName (queueName);
			if (queue != null) {
				return getQueue (queue);
			}
		}
		return null;
	}
	
	public QueueEntryInfo getQueueEntryByJobNumber (int jobNumber)
	{
		List<QueueEntryInfo> qentries = queues.values ().stream ().flatMap (List::stream)
				.filter (qe -> qe.getJobNumber () == jobNumber).collect (Collectors.toList ());
		if (qentries != null && qentries.size () > 0) {
			return qentries.get (0);
		}
		qentries = wcReader.getQueueEntryJobNumber (jobNumber);
		if (qentries != null && qentries.size () > 0) {
			return qentries.get (0);
		}
		return null;
	}
	
	@Override
	public EpmDocumentInfo getDocumentInfo (String docId)
	{
		return wcReader.getDocumentInfo (docId);
	}
	
	@Override
	public void getDocumentInfo (String docId, Consumer<EpmDocumentInfo> successHandler, Consumer<Throwable> errHandler)
	{
		wcReader.getDocumentInfo (docId, successHandler, errHandler);
	}
	
	@Override
	public List<WorkerInfo> getWorkers ()
	{
		return workers.stream ().collect (Collectors.toList ());
	}

	@Override
	public List<WorkerInfo> getBusyWorkers ()
	{
		return workers.getBusyWorkers ();
	}
	
	@Override
	public WorkerInfo getWorker (String displayName)
	{
		return workers.getWorkerByName (displayName);
	}
	

	
	/*
	 * Some QueueEntry status Info was changed in WC server
	 */
	public void statusChangeListener (QueueEntryInfo qentry)
	{
		if (wcReader != null) {
			qentry.setWindchillInfoReader (wcReader);
		}
		if (keepWorking) {
			if (curTime () + waitTimeout < nextRequestTime) {
				logDebug ("Processing status change listener event");
				scheduleWcRequest (waitTimeout);
			}
			else {
				logDebug ("Ignore consecutive status change listener event");
			}
			executor.execute (() -> {
				if (eventManager != null) {
					eventManager.dataUpdated (qentry);
				}
			});
		}
		else {
			logDebug ("Service has been terminated - ignoring status change listener event");
		}
	}
	
	public void workerEventListener (String eventType, WorkerInfo worker)
	{
		if (keepWorking) {
			logDebug ("Worker event: " + eventType + ": " + worker);
			switch (eventType) {
				case "WORKER_ADDED": 
					workers.add (worker);
					break;
				case "WORKER_REMOVED":
					workers.remove (worker);
					break;
				case "WORKER_JOB_STARTED":
					executor.execute (() -> printWorkerStarted (worker));
				case "WORKER_JOB_FINISHD":
					workers.setWorker (worker);
					break;
			}
			executor.execute (() -> {
				if (eventManager != null) {
					eventManager.dataUpdated (worker);
				}
				printWorkers ();
			});
		}
	}
	
	public void start ()
	{
		System.out.println ("PublishQueueMonitor starting");
		wcReader.getPublishQueueList (this::reviewPublishQueuesInfo, this::queueListErrHandler);
		wcReader.getAuthoringAppTypes (this::reviewAuthoringAppTypes, this::queueAuthTypesHandler);
		wcReader.addWorkerEventListener (this::registerCadWorkerListener, this::registerCadWorkerListenerErr);
		requestWcPublishQueues ();
		System.out.println ("PublishQueueMonitor started");
	}
	
	public void terminate ()
	{
		keepWorking = false;
		if (scheduledRequest != null) {
			scheduledRequest.cancel (true);
		}
		executor.shutdown ();
		System.out.println ("Publish Queue Monitor terminated");
	}
	
	@Override
	public List<QueueEntryInfo> getQueueEntries (Predicate<QueueEntryInfo> filter)
	{
		List<QueueEntryInfo> qlist = null;
		if (queues != null) {
			qlist = queues.values ().stream ()
					.flatMap (List::stream)
					.filter (filter)
					.collect (Collectors.toList ());
		}
		return qlist;
	}
	
	
	// ============================================================================
	private long curTime ()
	{
		return (new Date ().getTime ());
	}
	
	private synchronized void setPublishQueues (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		consecutiveQueueErrCount = 0;
		logDebug ("Received response for a queues request");
		executor.execute (() -> queuesInfoAnalysis (this.queues, queues));
		this.queues = queues;
	}
	
	private synchronized void setWorkers (List<WorkerInfo> workerList)
	{
		consecutiveWorkersErrCount = 0;
		logDebug ("Received response for a workers request");
		executor.execute (() -> workersInfoAnalysis (this.workers, workerList));
		this.workers.setWorkers (workerList);
	}
	
	private void errorHandler (Throwable err)
	{
		logError (err.getMessage () + " - errCount/maxErrCount = " + consecutiveQueueErrCount + "/" + maxConsecutiveErrCount);
		if (++consecutiveQueueErrCount >= maxConsecutiveErrCount) {
			terminate ();
		}
		scheduleWcRequest (errorResendTimeout);
	}
	
	private void queueCadWorkersHandler (Throwable err)
	{
		consecutiveWorkersErrCount ++;
		logError (String.format ("Failed to retrieve CAD Workers from Windchill - %s", err.getMessage ()));
		if (consecutiveWorkersErrCount >= maxConsecutiveErrCount) {
			requestWorkers = false;
		}
	}
	
	private void queueListErrHandler (Throwable err)
	{
		queueListErrCount ++;
		logError (String.format ("Failed to retrieve publisher queues from Windchill (%d/%d) - %s", 
				queueListErrCount, maxConsecutiveErrCount, err.getMessage ()));
		if (queueListErrCount < maxConsecutiveErrCount) {
			// TODO
		}
	}
	
	private void queueAuthTypesHandler (Throwable err)
	{
		logError (String.format ("Failed to retrieve Authoring Types from Windchill - %s", err.getMessage ()));
	}
	
	private void requestWcPublishQueues ()
	{
		scheduledRequest = null;
		wcReader.getQueues (this::setPublishQueues, this::errorHandler);
		logDebug ("Sent get publish queues request");
		if (requestWorkers) {
			wcReader.getCadWorkers (this::setWorkers, this::queueCadWorkersHandler);
		}
		logDebug ("Sent get CAD workers request");
		scheduleWcRequest (timeout);
	}
	
	private void reviewPublishQueuesInfo (List<QueueInfo> queueInfoList)
	{
		System.out.println ("Windchill Publisher queues:");
		for (QueueInfo qi : queueInfoList) {
			System.out.println ("  - " + qi);
		}
	}
	
	private void reviewAuthoringAppTypes (List<EpmAuthoringAppType> types)
	{
		System.out.println ("Received " + types.size () + " Authoring App Types");
	}
	
	private void registerCadWorkerListener (JsonObject reply)
	{
		if (reply.containsKey ("success") && reply.getBoolean ("success")) {
			System.out.println ("Sucecssfully registered micro-service as Windchill workers listener"); 
		}
		else {
			System.err.println ("Failed to register Windchill listener - " + reply.getString ("message", "reason unknown"));
		}
	}
	
	private void registerCadWorkerListenerErr (Throwable err)
	{
		logError (String.format ("Failed to register CAD Worker listener in Windchill - %s", err.getMessage ()));
	}
	
	private void scheduleWcRequest (long timeout)
	{
		if (scheduledRequest != null) {
			scheduledRequest.cancel (true);
		}
		if (keepWorking) {
			nextRequestTime = curTime () + timeout;
			scheduledRequest = executor.schedule (() -> requestWcPublishQueues (), timeout, TimeUnit.MILLISECONDS);
			logDebug ("Scheduled next request in " + timeout + " msec");
		}
	}
	
	private void queuesInfoAnalysis (HashMap<QueueInfo, List<QueueEntryInfo>> queuesA, 
			HashMap<QueueInfo, List<QueueEntryInfo>> queuesB)
	{
		if (eventManager != null) {
			eventManager.dataUpdated ();
		}
		System.out.println (new Date () + ": {");
		for (QueueInfo queueInfo : queuesB.keySet ()) {
			List<QueueEntryInfo> qlist = queuesB.get (queueInfo); 
			System.out.println ("   " + queueInfo + " - size = " + (qlist != null ? qlist.size () : 0));
		}
		System.out.println ("}");
		// ---
		if (RequestEpmDocuments && queuesB != null) {
			requestEpmDocuments (queuesB);
		}
		// ---
		if (queuesA == null || queuesB == null) {
			return;
		}
		if (QueuesDebugOutput) {
			saveQueuesToFile (queuesB);
		}
	}
	
	private void print (Collection<WorkerInfo> workersList)
	{
		System.out.println ("Workers: {");
		for (WorkerInfo worker : workersList) {
			System.out.println ("   " + worker);
		}
		System.out.println ("}");
	}
	
	private void workersInfoAnalysis (WorkersList workersA, List<WorkerInfo> workersList)
	{
		if (eventManager != null) {
			eventManager.workersUpdated ();
		}
		print (workersList);
	}
	
	private void printWorkers ()
	{
		print (workers);
	}
	
	private void printWorkerStarted (WorkerInfo worker)
	{
		System.out.println ("Worker " + worker + " started job servicing\n   " + 
				getQueueEntryByJobNumber (worker.getEntryNumber ()));
	}
	
	private void requestEpmDocuments (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		for (List<QueueEntryInfo> queue : queues.values ()) {
			for (QueueEntryInfo qentry : queue) {
				if (! qentry.hasDocumentInfo ()) {
					wcReader.getDocumentInfo (qentry.getPersistableRef (), doc -> processDocument (qentry, doc), 
							err -> processDocumentError (qentry, err));
				}
			}
		}
	}
	
	private void processDocument (QueueEntryInfo qentry, EpmDocumentInfo doc)
	{
		qentry.setDocumentInfo (doc);
		if (DocumentsDebugOutput) {
			logDebug ("Retrieved: " + doc.toString ());
		}
	}
	
	private void processDocumentError (QueueEntryInfo qentry, Throwable err)
	{
		logError ("Failed to retrieve document by ID=" + qentry.getPersistableRef () + " - " + err.getMessage ());
	}

	@SuppressWarnings ("resource")
	private void saveQueuesToFile (HashMap<QueueInfo, List<QueueEntryInfo>> queues)
	{
		Date dt = new Date ();
		File f = new File (dt.toString ().replace (" ", "_").replace (":", "-") + ".log");
		PrintStream out;
		try {
			out = new PrintStream (f);
			logDebug ("Queues' Info exported to " + f.getAbsolutePath ());
		}
		catch (FileNotFoundException e) {
			out = System.out;
			out.println (dt);
		}
		WindchillInfoReader.printQueues (queues, out);
	}
	
	private void logDebug (String msg)
	{
		System.out.println (new Date () + ": " + msg);
	}
	
	private void logError (String msg)
	{
		System.err.println (new Date () + ": " + msg);
	}

}
