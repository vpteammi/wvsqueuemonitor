package com.ptc.wvs.monitor.client;

import java.util.HashMap;

public class StatusInfoCode
{
	private static HashMap<String, StatusInfoCode> Values = new HashMap<> ();
	
	   /**
	    * Constant indicating that the entry is ready to be executed.
	    **/
	   public static final StatusInfoCode READY = new StatusInfoCode ("READY");

	   /**
	    * Constant indicating that the entry has been processed successfully.
	    **/
	   public static final StatusInfoCode COMPLETED = new StatusInfoCode ("COMPLETED");

	   /**
	    * Constant indicating that the entry has been processed unsuccessfully.
	    **/
	   public static final StatusInfoCode FAILED = new StatusInfoCode ("FAILED");

	   /**
	    * Constant indicating that the entry has been processed unsuccessfully
	    * but that normal failure processing should be bypassed.
	    *
	    * This status is only valid for process queue entries at this time
	    * and is only used specifically by WVS
	    **/
	   public static final StatusInfoCode NONEVENTFAILED = new StatusInfoCode ("NONEVENTFAILED");

	   /**
	    * Constant indicating that the entry has completed successfully
	    * but that we want to delete it even when the property 
	    * wt.queue.removeCompleted.queueName is set to false 
	    *
	    * This status is only valid for process queue entries at this time
	    * and is only used specifically by WVS
	    **/
	   public static final StatusInfoCode DELETECOMPLETED = new StatusInfoCode ("DELETECOMPLETED");

	   /**
	    * Constant to indicate that the entry is being executed.
	    **/
	   public static final StatusInfoCode EXECUTING = new StatusInfoCode ("EXECUTING");

	   /**
	    * State of an entry in which it is not chosen for processing.  An entry
	    * can only get into this state through operator intervention.  Normal processing
	    * ends up in the FAILED or COMPLETED states.
	    **/
	   public static final StatusInfoCode SUSPENDED = new StatusInfoCode ("SUSPENDED");

	   /**
	    * Used to indicate the entry should be rescheduled
	    **/
	   public static final StatusInfoCode RESCHEDULE = new StatusInfoCode ("RESCHEDULE");

	   /**
	    * Indicates that an entry has failed after repeated attempts and that
	    * an administrator should be notified.
	    **/
	   public static final StatusInfoCode NOTIFY = new StatusInfoCode ("NOTIFY");

	   /**
	    * Denotes that the QueueEntry is failed due to InvocationTargetException or Exception
	    **/
	   public static final StatusInfoCode SEVERE = new StatusInfoCode ("SEVERE");

	
	private String value;
	
	protected StatusInfoCode (String value)
	{
		super ();
		this.value = value;
		Values.put (value, this);
	}
	
	public static StatusInfoCode create (String value)
	{
		StatusInfoCode res = null;
		if (value != null) {
			res = Values.get (value);
			if (res == null) {
				res = new StatusInfoCode (value);
			}
		} 
		return res;
	}
	
	public static StatusInfoCode findByValue (String value)
	{
		StatusInfoCode res = null;
		if (value != null) {
			res = Values.get (value);
		}
		return res;
	}
	
	public String getValue ()
	{
		return value;
	}
	
	@Override
	public String toString ()
	{
		return value;
	}
	
}
