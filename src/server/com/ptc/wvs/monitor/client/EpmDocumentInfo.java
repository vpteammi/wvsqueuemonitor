package com.ptc.wvs.monitor.client;

import java.util.HashMap;

import io.vertx.core.json.JsonObject;

public class EpmDocumentInfo
{
	
	private static HashMap<String, EpmDocumentInfo> EpmDocs = new HashMap<> ();
	
	private String id;
	private String typeId;
	private String number;
	private String name;
	private String cadName;
	private String version;
	private String authoringApplication;
	
	/*
	 * {
     *   "id": "OR:wt.epm.EPMDocument:60316",
     *   "typeId": "WCTYPE|wt.epm.EPMDocument|org.qa.DefaultEPMDocument|org.qa.ECADDocument|org.qa.ECADDefinition",
     *   "attributes": {
     *     "number": "ECAD-HOOK-CREATE-VIEWABLE_TEMPLATE",
     *     "name": "ECAD Hook for Create Viewable",
     *     "version": "A.1",
     *     "CADName": "ECAD Hook for Create Viewable"
     *   }
     * }
	 */
	private EpmDocumentInfo (JsonObject json)
	{
		super ();
		id = getAttribute (json, "id");
		typeId = getAttribute (json, "typeId");
		if (json.containsKey ("attributes")) {
			JsonObject attrs = json.getJsonObject ("attributes");
			number = getAttribute (attrs, "number");
			name = getAttribute (attrs, "name");
			version = getAttribute (attrs, "version");
			cadName = getAttribute (attrs, "CADName");
			authoringApplication = getAttribute (attrs, "authoringApplication");
		}
	}
	
	public synchronized static EpmDocumentInfo create (JsonObject json)
	{
		String id = getAttribute (json, "id");
		if (id == null) {
			return null;
		}
		EpmDocumentInfo doc = EpmDocs.get (id);
		if (doc == null) {
			doc = new EpmDocumentInfo (json);
			EpmDocs.put (id, doc);
		}
		return doc;
	}
	
	public synchronized static EpmDocumentInfo find (String docId)
	{
		return EpmDocs.get (docId);
	}

	public String getId ()
	{
		return id;
	}

	public String getTypeId ()
	{
		return typeId;
	}

	public String getNumber ()
	{
		return number;
	}

	public String getName ()
	{
		return name;
	}

	public String getCadName ()
	{
		return cadName;
	}

	public String getVersion ()
	{
		return version;
	}
	
	public String getAuthoringApplication () 
	{
		return authoringApplication;
	}
	
	@Override
	public String toString ()
	{
		return String.format ("id: %s, number: %s:%s, name: %s, authApp: %s", id, number, version, name, authoringApplication);
	}
	
	public JsonObject toJson ()
	{
		JsonObject json = new JsonObject ()
				.put ("id", getId ())
				.put ("name", getName ())
				.put ("number", getNumber ())
				.put ("version", getVersion ())
				.put ("authoringApplication", getAuthoringApplication ());
		return json;
	}
	
	// ------------------------------------------
	private static String getAttribute (JsonObject json, String attrName)
	{
		String res = null;
		if (json.containsKey (attrName)) {
			try {
				res = json.getString (attrName);
			}
			catch (Throwable err) {
				System.err.println ("Faild to retrieve attribute '" + attrName + "' from JSON - " + err.getMessage ());
			}
		}
		return res;
	}
	
}
