package com.ptc.wvs.monitor.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.json.JsonObject;

public class QueueInfo
{
	public static final QueueInfo UnknownQueue = new QueueInfo ("null", "null");
	private static final HashMap<String, QueueInfo> QueuesByName = new HashMap<> ();
	private static final HashMap<String, QueueInfo> QueuesByID = new HashMap<> ();
	
	private String queueName;
	private String queueRef;
	
	private QueueInfo (String queueName, String queueRef)
	{
		super ();
		this.queueName = queueName;
		this.queueRef = queueRef;
	}
	
	public static QueueInfo create (String queueName, String queueRef)
	{
		if (queueName == null || queueRef == null) {
			return UnknownQueue;
		}
		String key = queueName;
		QueueInfo queue = QueuesByName.get (key);
		if (queue == null) {
			queue = new QueueInfo (queueName, queueRef);
			QueuesByName.put (key, queue);
			QueuesByID.put (queueRef, queue);
		}
		return queue;
	}
	
	public static QueueInfo createByRef (String queueRef)
	{
		if (queueRef == null) {
			return null;
		}
		return QueuesByID.get (queueRef);
	}
	
	public static QueueInfo create (JsonObject json)
	{
		String queueRef = (json.containsKey ("id") ? json.getString ("id") : null);
		String queueName = null;
		if (json.containsKey ("attributes")) {
			JsonObject attrs = json.getJsonObject ("attributes");
			queueName = (attrs.containsKey ("name") ? attrs.getString ("name") : null);
		}
		return QueueInfo.create (queueName, queueRef);
	}

	public static QueueInfo getQueueByName (String queueName)
	{
		if (queueName != null) {
			return QueuesByName.get (queueName);
		}
		return null;
	}

	public static QueueInfo[] getQueuesByName (String ... queueNames)
	{
		List<QueueInfo> queues = Arrays.stream (queueNames)
				.map (qn -> getQueueByName (qn))
				.filter (qi -> (qi != null))
				.collect (Collectors.toList ());
		return queues.toArray (new QueueInfo [queues.size ()]);
	}

	// ---------------------------------------------------------
	public String getQueueName ()
	{
		return queueName;
	}
	
	public String getQueueRef ()
	{
		return queueRef;
	}
	
	@Override
	public String toString ()
	{
		return toString (queueName, queueRef);
	}

	// ---------------------------------------------------------
	private static String toString (String name, String ref)
	{
		return String.format ("'%s' - %s", name, ref);
	}

}
